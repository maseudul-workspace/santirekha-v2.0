package com.webinfotech.santirekha2.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import com.webinfotech.santirekha2.AndroidApplication;
import com.webinfotech.santirekha2.domain.executors.Executor;
import com.webinfotech.santirekha2.domain.executors.MainThread;
import com.webinfotech.santirekha2.domain.interactors.AddAddressInteractor;
import com.webinfotech.santirekha2.domain.interactors.impl.AddAddressInteractorImpl;
import com.webinfotech.santirekha2.domain.models.User.UserInfo;
import com.webinfotech.santirekha2.presentation.presenters.AddAddressPresenter;
import com.webinfotech.santirekha2.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.santirekha2.repository.User.UserRepositoryImpl;

import es.dmoral.toasty.Toasty;

public class AddAddressPresenterImpl extends AbstractPresenter implements AddAddressPresenter, AddAddressInteractor.Callback {

    Context mContext;
    AddAddressPresenter.View mView;
    AndroidApplication androidApplication;
    AddAddressInteractorImpl addAddressInteractor;

    public AddAddressPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void addAddress(String mobile, String email, String state, String city, String address, String pin) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo != null) {
            addAddressInteractor = new AddAddressInteractorImpl(mExecutor, mMainThread, new UserRepositoryImpl(), this, userInfo.userId, userInfo.apiKey, mobile, email, state, city, address, pin);
            addAddressInteractor.execute();
            mView.showLoader();
        } else {
            Toasty.warning(mContext, "Your Session Is Expired!! Please Login Again").show();
        }
    }

    @Override
    public void onAddressAddSuccess() {
        mView.hideLoader();
        Toasty.success(mContext, "Address Added Successfully", Toast.LENGTH_SHORT).show();
        mView.onAddressAddSuccess();
    }

    @Override
    public void onAddAddressFail(String errorMsg) {
        mView.hideLoader();
        Toasty.error(mContext, errorMsg, Toast.LENGTH_SHORT).show();
    }
}
