package com.webinfotech.santirekha2.presentation.presenters.impl;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.webinfotech.santirekha2.AndroidApplication;
import com.webinfotech.santirekha2.domain.executors.Executor;
import com.webinfotech.santirekha2.domain.executors.MainThread;
import com.webinfotech.santirekha2.domain.executors.impl.ThreadExecutor;
import com.webinfotech.santirekha2.domain.interactors.FetchReviewsInteractor;
import com.webinfotech.santirekha2.domain.interactors.FetchStarProductsCountInteractor;
import com.webinfotech.santirekha2.domain.interactors.GetCartDetailsInteractor;
import com.webinfotech.santirekha2.domain.interactors.GetCategoryListInteractor;
import com.webinfotech.santirekha2.domain.interactors.GetMainDataInteractor;
import com.webinfotech.santirekha2.domain.interactors.GetWalletStatusInteractor;
import com.webinfotech.santirekha2.domain.interactors.SubmitReviewInteractor;
import com.webinfotech.santirekha2.domain.interactors.impl.FetchReviewsInteractorImpl;
import com.webinfotech.santirekha2.domain.interactors.impl.FetchStarProductsCountInteractorImpl;
import com.webinfotech.santirekha2.domain.interactors.impl.GetCartDetailsInteractorImpl;
import com.webinfotech.santirekha2.domain.interactors.impl.GetCategoryListInteractorImpl;
import com.webinfotech.santirekha2.domain.interactors.impl.GetMainDataInteractorImpl;
import com.webinfotech.santirekha2.domain.interactors.impl.GetWalletStatusInteractorImpl;
import com.webinfotech.santirekha2.domain.interactors.impl.SubmitReviewInteractorImpl;
import com.webinfotech.santirekha2.domain.models.Cart.Cart;
import com.webinfotech.santirekha2.domain.models.Category.Category;
import com.webinfotech.santirekha2.domain.models.ImageSliders.ImageSlider;
import com.webinfotech.santirekha2.domain.models.MainData;
import com.webinfotech.santirekha2.domain.models.Review.Review;
import com.webinfotech.santirekha2.domain.models.User.UserInfo;
import com.webinfotech.santirekha2.domain.models.Wallet.WalletStatus;
import com.webinfotech.santirekha2.presentation.presenters.MainPresenter;
import com.webinfotech.santirekha2.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.santirekha2.presentation.ui.adapters.HomeSlidingImagesAdapter;
import com.webinfotech.santirekha2.presentation.ui.adapters.MainCategoriesAdapter;
import com.webinfotech.santirekha2.presentation.ui.adapters.ProductHorizontalAdapter;
import com.webinfotech.santirekha2.presentation.ui.adapters.ReviewsAdapter;
import com.webinfotech.santirekha2.repository.Category.CategoryRepositoryImpl;
import com.webinfotech.santirekha2.repository.ProductRepository.impl.CartRepositoryImpl;
import com.webinfotech.santirekha2.repository.ProductRepository.impl.GetProductsRepositoryImpl;
import com.webinfotech.santirekha2.repository.User.UserRepositoryImpl;
import com.webinfotech.santirekha2.threading.MainThreadImpl;

import es.dmoral.toasty.Toasty;

public class MainPresenterImpl extends AbstractPresenter implements MainPresenter,
                                                                    GetCategoryListInteractor.Callback,
                                                                    MainCategoriesAdapter.Callback,
                                                                    GetMainDataInteractor.Callback,
                                                                    ProductHorizontalAdapter.Callback,
                                                                    GetCartDetailsInteractor.Callback,
                                                                    FetchStarProductsCountInteractor.Callback,
                                                                    GetWalletStatusInteractor.Callback,
                                                                    FetchReviewsInteractor.Callback,
                                                                    SubmitReviewInteractor.Callback

{

    Context mContext;
    MainPresenter.View mView;
    GetCategoryListInteractorImpl getCategoryListInteractor;
    GetMainDataInteractorImpl getMainDataInteractor;
    GetCartDetailsInteractorImpl getCartDetailsInteractor;
    FetchStarProductsCountInteractorImpl fetchStarProductsCountInteractor;

    public MainPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void getCategories() {
        getCategoryListInteractor = new GetCategoryListInteractorImpl(mExecutor, mMainThread, new CategoryRepositoryImpl(), this);
        getCategoryListInteractor.execute();
    }

    @Override
    public void getMainData() {
        getMainDataInteractor = new GetMainDataInteractorImpl(mExecutor, mMainThread, new GetProductsRepositoryImpl(), this);
        getMainDataInteractor.execute();
    }

    @Override
    public void fetchCartCount() {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo != null) {
            getCartDetailsInteractor = new GetCartDetailsInteractorImpl(mExecutor,
                    mMainThread,
                    new CartRepositoryImpl(),
                    this,
                    userInfo.userId,
                    userInfo.apiKey);
            getCartDetailsInteractor.execute();
        }
    }

    @Override
    public void fetchStarProductsCount() {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo != null) {
            fetchStarProductsCountInteractor = new FetchStarProductsCountInteractorImpl(mExecutor, mMainThread, new UserRepositoryImpl(), this, userInfo.userId, userInfo.apiKey);
            fetchStarProductsCountInteractor.execute();
        }
    }

    @Override
    public void fetchWalletAmount() {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo != null) {
            GetWalletStatusInteractorImpl getWalletStatusInteractor = new GetWalletStatusInteractorImpl(mExecutor, mMainThread, this, new UserRepositoryImpl(), userInfo.apiKey, userInfo.userId);
            getWalletStatusInteractor.execute();
        }
    }

    @Override
    public void submitReview(int rating, String comments) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo != null) {
            SubmitReviewInteractorImpl submitReviewInteractor = new SubmitReviewInteractorImpl(mExecutor, mMainThread, this, new UserRepositoryImpl(), userInfo.userId, userInfo.apiKey, rating, comments);
            submitReviewInteractor.execute();
            mView.showLoader();
        } else {
            Toasty.warning(mContext, "Please login to submit review", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void fetchReviews() {
        FetchReviewsInteractorImpl fetchReviewsInteractor = new FetchReviewsInteractorImpl(mExecutor, mMainThread, this, new GetProductsRepositoryImpl());
        fetchReviewsInteractor.execute();
    }

    @Override
    public void onGettingCategoryListSuccess(Category[] categories) {
        MainCategoriesAdapter mainCategoriesAdapter = new MainCategoriesAdapter(mContext, categories, this);
        mView.loadCategoriesAdapter(mainCategoriesAdapter);
    }

    @Override
    public void onGettingCategoryFail(String errorMsg) {

    }

    @Override
    public void onCategoryClicked(int catId, String categoryName, boolean subcategoryStatus) {
        if (subcategoryStatus) {
            mView.goToSubcategoryActivity(catId, categoryName);
        } else {
            mView.goToProductList(catId, categoryName);
        }
    }

    @Override
    public void onGettingMainDataSuccess(MainData mainData) {
        ProductHorizontalAdapter newProductsAdapter = new ProductHorizontalAdapter(mContext, mainData.newArrivals, this);
        ProductHorizontalAdapter starProductsAdapter = new ProductHorizontalAdapter(mContext, mainData.startProducts, this);
        mView.loadMainData(newProductsAdapter, starProductsAdapter, mainData.imageSliders);
    }

    @Override
    public void onGettingMainDataFail(String errorMsg) {

    }

    @Override
    public void onProductClicked(int productId) {
        mView.goToProductDetails(productId);
    }

    @Override
    public void onGetCartDetailsSuccess(Cart[] carts) {
        mView.loadCartCount(carts.length);
    }

    @Override
    public void onGetCartDetailsFail(String errorMsg) {
        mView.loadCartCount(0);
    }

    @Override
    public void onGettingStarProductsCountSuccess(int starProductsCount) {
        mView.loadStarProductsCount(starProductsCount);
    }

    @Override
    public void onGettingStarProductsCountFail(String errorMsg) {
        mView.loadStarProductsCount(0);
    }

    @Override
    public void onGettingWalletStatusSuccess(WalletStatus walletStatus) {
        mView.loadWalletAmount(walletStatus.totalAmount);
    }

    @Override
    public void onGettingWalletStatusFail(String errorMsg) {

    }

    @Override
    public void onGettingReviewsSuccess(Review[] reviews) {
        Log.e("LogMsg", "Review");
        ReviewsAdapter reviewsAdapter = new ReviewsAdapter(mContext, reviews);
        mView.loadReviews(reviewsAdapter);
    }

    @Override
    public void onGettingReviewsFail() {

    }

    @Override
    public void onSubmitReviewSuccess(String successMsg) {
        Toasty.success(mContext, "Your review is submitted").show();
        mView.hideLoader();
        fetchReviews();
    }

    @Override
    public void onSubmitReviewFail(String errorMsg) {
        Toasty.warning(mContext, errorMsg).show();
        mView.hideLoader();
    }
}
