package com.webinfotech.santirekha2.presentation.presenters;

public interface LoginPresenter {
    void checkLogin(String email, String password);
    interface View {
        void onLoginSuccess();
        void showLoader();
        void hideLoader();
    }
}
