package com.webinfotech.santirekha2.presentation.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.webinfotech.santirekha2.R;
import com.webinfotech.santirekha2.domain.models.Wallet.WalletHistory;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class WalletHistoryAdapter extends RecyclerView.Adapter<WalletHistoryAdapter.ViewHolder> {

    Context mContext;
    WalletHistory[] walletHistories;

    public WalletHistoryAdapter(Context mContext, WalletHistory[] walletHistories) {
        this.mContext = mContext;
        this.walletHistories = walletHistories;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_wallet_history, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        if (walletHistories[position].transaction_type == 1) {
            holder.layoutDebit.setVisibility(View.VISIBLE);
            holder.layoutCredit.setVisibility(View.GONE);
            holder.txtViewWalletTransactionAmount.setTextColor(mContext.getResources().getColor(R.color.md_pink_500));
            holder.txtViewWalletTransactionAmount.setText("-  ₹ " + walletHistories[position].transaction_amount);
        } else {
            holder.layoutDebit.setVisibility(View.GONE);
            holder.layoutCredit.setVisibility(View.VISIBLE);
            holder.txtViewWalletTransactionAmount.setTextColor(mContext.getResources().getColor(R.color.md_teal_300));
            holder.txtViewWalletTransactionAmount.setText("+  ₹ " + walletHistories[position].transaction_amount);
        }
        holder.txtViewWalletMessage.setText(walletHistories[position].comment);
    }

    @Override
    public int getItemCount() {
        return walletHistories.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.txt_view_wallet_message)
        TextView txtViewWalletMessage;
        @BindView(R.id.txt_view_wallet_transaction_amount)
        TextView txtViewWalletTransactionAmount;
        @BindView(R.id.layout_debit)
        View layoutDebit;
        @BindView(R.id.layout_credit)
        View layoutCredit;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }

    public void updateDataSet(WalletHistory[] walletHistories) {
        this.walletHistories = walletHistories;
    }

}
