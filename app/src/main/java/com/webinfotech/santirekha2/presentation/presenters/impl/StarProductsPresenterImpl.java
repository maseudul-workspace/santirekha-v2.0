package com.webinfotech.santirekha2.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import com.webinfotech.santirekha2.AndroidApplication;
import com.webinfotech.santirekha2.domain.executors.Executor;
import com.webinfotech.santirekha2.domain.executors.MainThread;
import com.webinfotech.santirekha2.domain.interactors.FetchStarProductsInteractor;
import com.webinfotech.santirekha2.domain.interactors.impl.FetchStarProductsInteractorImpl;
import com.webinfotech.santirekha2.domain.models.Products.Product;
import com.webinfotech.santirekha2.domain.models.Products.StarProduct;
import com.webinfotech.santirekha2.domain.models.User.UserInfo;
import com.webinfotech.santirekha2.presentation.presenters.StarProductsPresenter;
import com.webinfotech.santirekha2.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.santirekha2.presentation.ui.adapters.ProductListVerticalAdapter;
import com.webinfotech.santirekha2.presentation.ui.adapters.StarProductsAdapter;
import com.webinfotech.santirekha2.repository.ProductRepository.impl.GetProductsRepositoryImpl;

import es.dmoral.toasty.Toasty;

public class StarProductsPresenterImpl extends AbstractPresenter implements StarProductsPresenter, FetchStarProductsInteractor.Callback, StarProductsAdapter.Callback {

    Context mContext;
    StarProductsPresenter.View mView;
    FetchStarProductsInteractorImpl fetchStarProductsInteractor;
    StarProduct[] newStarProducts;
    StarProductsAdapter adapter;
    AndroidApplication androidApplication;

    public StarProductsPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchStarProducts(int pageNo, String type) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo == null) {
            Toasty.warning(mContext, "Your Session Expired", Toast.LENGTH_SHORT).show();
        } else {
            if (type.equals("refresh")) {
                newStarProducts = null;
            }
            fetchStarProductsInteractor = new FetchStarProductsInteractorImpl(mExecutor, mMainThread, new GetProductsRepositoryImpl(), this, userInfo.userId, userInfo.apiKey, pageNo);
            fetchStarProductsInteractor.execute();
            mView.showLoader();
        }
    }

    @Override
    public void onGettingStarProductsSuccess(StarProduct[] starProducts, int totalPage) {
        StarProduct[] tempStarProducts;
        tempStarProducts = newStarProducts;
        try {
            int len1 = tempStarProducts.length;
            int len2 = starProducts.length;
            newStarProducts = new StarProduct[len1 + len2];
            System.arraycopy(tempStarProducts, 0, newStarProducts, 0, len1);
            System.arraycopy(starProducts, 0, newStarProducts, len1, len2);
            adapter.updateData(newStarProducts);
            adapter.notifyDataSetChanged();
        }catch (NullPointerException e){
            newStarProducts = starProducts;
            adapter = new StarProductsAdapter(mContext, starProducts, this);
            mView.loadData(adapter, totalPage);
        }
        mView.hideLoader();
    }

    @Override
    public void onGettingStarProductsFail(String errorMsg) {
        mView.hideLoader();
        Toasty.error(mContext, errorMsg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onProductClicked(int productId) {

    }
}
