package com.webinfotech.santirekha2.presentation.presenters;

public interface AddAddressPresenter {
    void addAddress(String mobile, String email, String state, String city, String address, String pin);
    interface View {
        void onAddressAddSuccess();
        void showLoader();
        void hideLoader();
    }
}
