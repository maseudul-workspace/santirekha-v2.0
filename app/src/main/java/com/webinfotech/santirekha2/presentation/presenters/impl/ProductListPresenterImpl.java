package com.webinfotech.santirekha2.presentation.presenters.impl;

import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import com.webinfotech.santirekha2.AndroidApplication;
import com.webinfotech.santirekha2.domain.executors.Executor;
import com.webinfotech.santirekha2.domain.executors.MainThread;
import com.webinfotech.santirekha2.domain.interactors.AddToCartInteractor;
import com.webinfotech.santirekha2.domain.interactors.GetProductListInteractor;
import com.webinfotech.santirekha2.domain.interactors.impl.AddToCartInteractorImpl;
import com.webinfotech.santirekha2.domain.interactors.impl.FetchShippingAddressInteractorImpl;
import com.webinfotech.santirekha2.domain.interactors.impl.GetProductListInteractorImpl;
import com.webinfotech.santirekha2.domain.models.Products.Product;
import com.webinfotech.santirekha2.domain.models.User.UserInfo;
import com.webinfotech.santirekha2.presentation.presenters.ProductListPresenter;
import com.webinfotech.santirekha2.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.santirekha2.presentation.ui.adapters.ProductListVerticalAdapter;
import com.webinfotech.santirekha2.repository.ProductRepository.impl.CartRepositoryImpl;
import com.webinfotech.santirekha2.repository.ProductRepository.impl.GetProductsRepositoryImpl;
import com.webinfotech.santirekha2.repository.User.UserRepositoryImpl;

import es.dmoral.toasty.Toasty;

public class ProductListPresenterImpl extends AbstractPresenter implements  ProductListPresenter,
                                                                            GetProductListInteractor.Callback,
                                                                            ProductListVerticalAdapter.Callback,
                                                                            AddToCartInteractor.Callback
{

    Context mContext;
    ProductListPresenter.View mView;
    GetProductListInteractorImpl getProductListInteractor;
    ProductListVerticalAdapter productListVerticalAdapter;
    Product[] newProducts;
    AddToCartInteractorImpl addToCartInteractor;
    AndroidApplication androidApplication;

    public ProductListPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchProductList(int type, int pageNo, int categoryId, String refresh) {
        if (refresh.equals("refresh")) {
            newProducts = null;
        }
        getProductListInteractor = new GetProductListInteractorImpl(mExecutor, mMainThread, new GetProductsRepositoryImpl(), this, type, pageNo, categoryId);
        getProductListInteractor.execute();
    }

    @Override
    public void onGettingProductListSuccess(Product[] products, int totalPage) {
        Product[] tempProducts;
        tempProducts = newProducts;
        try {
            int len1 = tempProducts.length;
            int len2 = products.length;
            newProducts = new Product[len1 + len2];
            System.arraycopy(tempProducts, 0, newProducts, 0, len1);
            System.arraycopy(products, 0, newProducts, len1, len2);
            productListVerticalAdapter.updateDataset(newProducts);
            productListVerticalAdapter.notifyDataSetChanged();
        }catch (NullPointerException e){
            newProducts = products;
            productListVerticalAdapter = new ProductListVerticalAdapter(mContext, products, this);
            mView.loadProductListAdapter(productListVerticalAdapter, totalPage);
        }
        mView.hideLoader();
    }

    @Override
    public void onGettingProductListFail(String errorMsg) {
        mView.hideLoader();
        mView.onNoProductsFound();
    }

    @Override
    public void onProductClicked(int productId) {
        mView.goToProductDetails(productId);
    }

    @Override
    public void onAddToCartClicked(int productId) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo != null) {
            addToCartInteractor = new AddToCartInteractorImpl(mExecutor, mMainThread, new CartRepositoryImpl(), this, userInfo.apiKey, productId, userInfo.userId, 1);
            addToCartInteractor.execute();
            mView.showLoader();
        } else {
            mView.showLoginSnackbar();
        }
    }

    @Override
    public void onAddToCartSuccess(String successMsg) {
        mView.hideLoader();
        mView.showGoToCartSnackbar();
    }

    @Override
    public void onAddToCartFail(String errorMsg) {
        mView.hideLoader();
        Toasty.error(mContext, errorMsg, Toast.LENGTH_SHORT).show();
    }
}
