package com.webinfotech.santirekha2.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import com.webinfotech.santirekha2.AndroidApplication;
import com.webinfotech.santirekha2.domain.executors.Executor;
import com.webinfotech.santirekha2.domain.executors.MainThread;
import com.webinfotech.santirekha2.domain.interactors.ChangePasswordInteractor;
import com.webinfotech.santirekha2.domain.interactors.impl.ChangePasswordInteractorImpl;
import com.webinfotech.santirekha2.domain.models.User.UserInfo;
import com.webinfotech.santirekha2.presentation.presenters.ChangePasswordPresenter;
import com.webinfotech.santirekha2.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.santirekha2.repository.User.UserRepositoryImpl;

import es.dmoral.toasty.Toasty;

/**
 * Created by Raj on 14-02-2019.
 */

public class ChangePasswordPresenterImpl extends AbstractPresenter implements ChangePasswordPresenter, ChangePasswordInteractor.Callback{

    Context mContext;
    ChangePasswordInteractorImpl mInteractor;
    ChangePasswordPresenter.View mView;
    AndroidApplication androidApplication;

    public ChangePasswordPresenterImpl(Executor executor,
                                       MainThread mainThread,
                                       Context context,
                                       ChangePasswordPresenter.View view
                                       ) {
        super(executor, mainThread);
        this.mView = view;
        this.mContext = context;
    }

    @Override
    public void changePassword(String newPassword, String oldPassword) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo != null) {
            mInteractor = new ChangePasswordInteractorImpl(mExecutor, mMainThread, this,
                    new UserRepositoryImpl(), newPassword, oldPassword, userInfo.apiKey, userInfo.userId);
            mInteractor.execute();
        } else {
            Toasty.warning(mContext, "Your Session Is Expired!! Please Login Again").show();
        }
    }

    @Override
    public void onChangingPasswordSuccess(String successMsg) {
        mView.hideProgressBar();
        Toasty.success(mContext, successMsg, Toast.LENGTH_SHORT).show();
        mView.onChangePasswordSuccess();
    }

    @Override
    public void onChangingPasswordFail(String errorMsg) {
        mView.hideProgressBar();
        Toasty.error(mContext, errorMsg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void stop() {

    }

    @Override
    public void destroy() {

    }
}
