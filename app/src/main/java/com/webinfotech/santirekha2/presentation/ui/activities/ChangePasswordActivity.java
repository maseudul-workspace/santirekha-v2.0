package com.webinfotech.santirekha2.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.EditText;

import com.google.android.material.textfield.TextInputLayout;
import com.webinfotech.santirekha2.R;
import com.webinfotech.santirekha2.domain.executors.impl.ThreadExecutor;
import com.webinfotech.santirekha2.presentation.presenters.ChangePasswordPresenter;
import com.webinfotech.santirekha2.presentation.presenters.impl.ChangePasswordPresenterImpl;
import com.webinfotech.santirekha2.threading.MainThreadImpl;

public class ChangePasswordActivity extends AppCompatActivity implements ChangePasswordPresenter.View {

    @BindView(R.id.edit_text_new_password)
    EditText editTextNewPassword;
    @BindView(R.id.edit_text_old_password)
    EditText editTextOldPassword;
    @BindView(R.id.txt_input_new_password_layout)
    TextInputLayout txtInputNewPasswordLayout;
    @BindView(R.id.txt_input_old_password_layout)
    TextInputLayout txtInputOldPasswordLayout;
    ProgressDialog progressDialog;
    ChangePasswordPresenterImpl mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        getSupportActionBar().setTitle("Change Password");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ButterKnife.bind(this);
        setUpProgressDialog();
        initialisePresenter();
    }

    private void initialisePresenter() {
        mPresenter = new ChangePasswordPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    private void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    @Override
    public void onChangePasswordSuccess() {
        editTextNewPassword.setText("");
        editTextOldPassword.setText("");
    }

    @Override
    public void loadProgressBar() {
        progressDialog.show();
    }

    @Override
    public void hideProgressBar() {
        progressDialog.dismiss();
    }

    @OnClick(R.id.btn_change_password) void onChangePasswordClicked() {
        txtInputNewPasswordLayout.setError("");
        txtInputOldPasswordLayout.setError("");
        if (editTextNewPassword.getText().toString().trim().isEmpty()) {
            txtInputNewPasswordLayout.setError("Please Enter New Password");
        } else if (editTextOldPassword.getText().toString().trim().isEmpty()) {
            txtInputOldPasswordLayout.setError("Please Enter Old Password");
        } else {
            mPresenter.changePassword(editTextNewPassword.getText().toString(), editTextOldPassword.getText().toString());
            loadProgressBar();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


}
