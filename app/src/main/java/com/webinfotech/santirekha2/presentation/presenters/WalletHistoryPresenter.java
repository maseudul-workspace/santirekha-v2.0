package com.webinfotech.santirekha2.presentation.presenters;

import com.webinfotech.santirekha2.domain.models.Wallet.WalletDetails;
import com.webinfotech.santirekha2.presentation.ui.adapters.WalletHistoryAdapter;

public interface WalletHistoryPresenter {
    void fetchWalletHistory(int page, String type);
    interface View {
        void loadAdapter(WalletHistoryAdapter adapter, WalletDetails walletDetails, int totalPage);
        void showLoader();
        void hideLoader();
    }
}
