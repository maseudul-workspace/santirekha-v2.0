package com.webinfotech.santirekha2.presentation.presenters;

import com.webinfotech.santirekha2.presentation.presenters.base.BasePresenter;
import com.webinfotech.santirekha2.presentation.ui.adapters.OrdersAdapter;

/**
 * Created by Raj on 22-02-2019.
 */

public interface OrderHistoryPresenter extends BasePresenter {
    void getOrderHistory();
    interface View{
        void loadOrdersAdapter(OrdersAdapter adapter);
        void showProgressBar();
        void hideProgressBar();
    }
}
