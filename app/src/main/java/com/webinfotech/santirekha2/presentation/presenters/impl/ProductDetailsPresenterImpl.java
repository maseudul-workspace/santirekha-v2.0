package com.webinfotech.santirekha2.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import com.webinfotech.santirekha2.AndroidApplication;
import com.webinfotech.santirekha2.domain.executors.Executor;
import com.webinfotech.santirekha2.domain.executors.MainThread;
import com.webinfotech.santirekha2.domain.interactors.AddToCartInteractor;
import com.webinfotech.santirekha2.domain.interactors.FetchProductDetailsInteractor;
import com.webinfotech.santirekha2.domain.interactors.GetProductListInteractor;
import com.webinfotech.santirekha2.domain.interactors.impl.AddToCartInteractorImpl;
import com.webinfotech.santirekha2.domain.interactors.impl.FetchProductDetailsInteractorImpl;
import com.webinfotech.santirekha2.domain.interactors.impl.GetProductListInteractorImpl;
import com.webinfotech.santirekha2.domain.models.Products.Product;
import com.webinfotech.santirekha2.domain.models.Products.ProductDetailsData;
import com.webinfotech.santirekha2.domain.models.User.UserInfo;
import com.webinfotech.santirekha2.presentation.presenters.ProductDetailsPresenter;
import com.webinfotech.santirekha2.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.santirekha2.presentation.ui.adapters.ProductHorizontalAdapter;
import com.webinfotech.santirekha2.repository.ProductRepository.impl.CartRepositoryImpl;
import com.webinfotech.santirekha2.repository.ProductRepository.impl.GetProductsRepositoryImpl;

import es.dmoral.toasty.Toasty;

public class ProductDetailsPresenterImpl extends AbstractPresenter implements ProductDetailsPresenter, FetchProductDetailsInteractor.Callback, AddToCartInteractor.Callback, ProductHorizontalAdapter.Callback {

    Context mContext;
    ProductDetailsPresenter.View mView;
    FetchProductDetailsInteractorImpl fetchProductDetailsInteractor;
    AddToCartInteractorImpl addToCartInteractor;
    AndroidApplication androidApplication;

    public ProductDetailsPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchProductDetails(int productId) {
        fetchProductDetailsInteractor = new FetchProductDetailsInteractorImpl(mExecutor, mMainThread, new GetProductsRepositoryImpl(), this, productId);
        fetchProductDetailsInteractor.execute();
    }

    @Override
    public void addToCart(int productId, int quantity) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo != null) {
            addToCartInteractor = new AddToCartInteractorImpl(mExecutor, mMainThread, new CartRepositoryImpl(), this, userInfo.apiKey, productId, userInfo.userId, quantity);
            addToCartInteractor.execute();
            mView.showLoader();
        } else {
            mView.showLoginSnackbar();
        }
    }

    @Override
    public void onGettingProductDetailsSuccess(ProductDetailsData product) {
        ProductHorizontalAdapter adapter = null;
        if (product.products != null) {
            adapter = new ProductHorizontalAdapter(mContext, product.products, this);
        }
        mView.loadProductDetails(product, adapter);
        mView.hideLoader();
    }

    @Override
    public void onGettingProductDetailsFail(String errorMsg) {
        mView.hideLoader();
        Toasty.error(mContext, errorMsg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onAddToCartSuccess(String successMsg) {
        mView.hideLoader();
        mView.showCartSnackbar();
    }

    @Override
    public void onAddToCartFail(String errorMsg) {
        mView.hideLoader();
        Toasty.error(mContext, errorMsg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onProductClicked(int productId) {
        mView.goToProductDetails(productId);
    }
}
