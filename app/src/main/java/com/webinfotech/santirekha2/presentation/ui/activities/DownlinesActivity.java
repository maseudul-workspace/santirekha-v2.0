package com.webinfotech.santirekha2.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.MenuItem;

import com.webinfotech.santirekha2.R;
import com.webinfotech.santirekha2.domain.executors.impl.ThreadExecutor;
import com.webinfotech.santirekha2.presentation.presenters.DownlineActivityPresenter;
import com.webinfotech.santirekha2.presentation.presenters.impl.DownlineActivityPresenterImpl;
import com.webinfotech.santirekha2.presentation.ui.adapters.DownlinesAdapter;
import com.webinfotech.santirekha2.threading.MainThreadImpl;

public class DownlinesActivity extends AppCompatActivity implements DownlineActivityPresenter.View {

    @BindView(R.id.recycler_view_downlines)
    RecyclerView recyclerViewDownlines;
    ProgressDialog progressDialog;
    DownlineActivityPresenterImpl mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_downlines);
        ButterKnife.bind(this);
        getSupportActionBar().setTitle("Downlines");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setUpProgressDialog();
        initialisePresenter();
        mPresenter.getDownlineList();
    }

    private void initialisePresenter() {
        mPresenter = new DownlineActivityPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    private void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    @Override
    public void showProgressBar() {
        progressDialog.show();
    }

    @Override
    public void hideProgressBar() {
        progressDialog.dismiss();
    }

    @Override
    public void loadData(DownlinesAdapter adapter) {
        recyclerViewDownlines.setAdapter(adapter);
        recyclerViewDownlines.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


}
