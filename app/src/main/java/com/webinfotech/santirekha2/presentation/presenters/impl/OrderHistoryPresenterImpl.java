package com.webinfotech.santirekha2.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import com.webinfotech.santirekha2.AndroidApplication;
import com.webinfotech.santirekha2.domain.executors.Executor;
import com.webinfotech.santirekha2.domain.executors.MainThread;
import com.webinfotech.santirekha2.domain.interactors.GetOrderHistoryInteractor;
import com.webinfotech.santirekha2.domain.interactors.impl.GetOrderHistoryInteractorImpl;
import com.webinfotech.santirekha2.domain.models.Orders.OrderHistory;
import com.webinfotech.santirekha2.domain.models.User.UserInfo;
import com.webinfotech.santirekha2.presentation.presenters.OrderHistoryPresenter;
import com.webinfotech.santirekha2.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.santirekha2.presentation.ui.adapters.OrdersAdapter;
import com.webinfotech.santirekha2.repository.User.UserRepositoryImpl;

import es.dmoral.toasty.Toasty;


/**
 * Created by Raj on 22-02-2019.
 */

public class OrderHistoryPresenterImpl extends AbstractPresenter implements OrderHistoryPresenter,
                                                                            GetOrderHistoryInteractor.Callback
{

    Context mContext;
    GetOrderHistoryInteractorImpl mInteractor;
    OrderHistoryPresenter.View mView;
    AndroidApplication androidApplication;
    UserInfo userInfo;
    OrdersAdapter ordersHistoryAdapter;

    public OrderHistoryPresenterImpl(Executor executor,
                                     MainThread mainThread,
                                     Context context,
                                     OrderHistoryPresenter.View view
                                     ) {
        super(executor, mainThread);
        this.mContext = context;
        this.mView = view;
    }

    @Override
    public void getOrderHistory() {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo != null) {
            mInteractor = new GetOrderHistoryInteractorImpl(mExecutor, mMainThread, this, new UserRepositoryImpl(), userInfo.apiKey, userInfo.userId);
            mInteractor.execute();
            mView.showProgressBar();
        } else {
            Toasty.warning(mContext, "Your Session Is Expired!! Please Login Again").show();
        }
    }

    @Override
    public void onGettingOrderHistorySuccess(OrderHistory[] orderHistories) {
        ordersHistoryAdapter = new OrdersAdapter(mContext, orderHistories);
        mView.loadOrdersAdapter(ordersHistoryAdapter);
        mView.hideProgressBar();
    }

    @Override
    public void onGettingOrderHistoryFail(String errorMsg) {
        mView.hideProgressBar();
        Toast.makeText(mContext, errorMsg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void stop() {

    }

    @Override
    public void destroy() {

    }
}
