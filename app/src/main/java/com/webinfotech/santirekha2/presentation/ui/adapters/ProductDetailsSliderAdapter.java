package com.webinfotech.santirekha2.presentation.ui.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.webinfotech.santirekha2.domain.models.Testing.HomeSliders;
import com.webinfotech.santirekha2.util.GlideHelper;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

public class ProductDetailsSliderAdapter extends PagerAdapter {

    Context mContext;
    ArrayList<HomeSliders> homeSliders;

    public ProductDetailsSliderAdapter(Context mContext, ArrayList<HomeSliders> homeSliders) {
        this.mContext = mContext;
        this.homeSliders = homeSliders;
    }

    @Override
    public int getCount() {
        return homeSliders.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        ImageView imageView = new ImageView(mContext);
        GlideHelper.setImageView(mContext, imageView, homeSliders.get(position).imageUrl);
        container.addView(imageView);
        return imageView;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }

}
