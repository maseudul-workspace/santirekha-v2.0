package com.webinfotech.santirekha2.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import com.webinfotech.santirekha2.domain.executors.Executor;
import com.webinfotech.santirekha2.domain.executors.MainThread;
import com.webinfotech.santirekha2.domain.interactors.ForgetPasswordInteractor;
import com.webinfotech.santirekha2.domain.interactors.impl.ForgetPasswordInteractorImpl;
import com.webinfotech.santirekha2.presentation.presenters.ForgetPasswordPresenter;
import com.webinfotech.santirekha2.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.santirekha2.repository.User.UserRepositoryImpl;

import es.dmoral.toasty.Toasty;

/**
 * Created by Raj on 25-06-2019.
 */

public class ForgetPasswordPresenterImpl extends AbstractPresenter implements ForgetPasswordPresenter, ForgetPasswordInteractor.Callback {

    Context mContext;
    ForgetPasswordPresenter.View mView;
    ForgetPasswordInteractorImpl mInteractor;

    public ForgetPasswordPresenterImpl(Executor executor,
                                       MainThread mainThread,
                                       Context context,
                                       ForgetPasswordPresenter.View view
                                       ) {
        super(executor, mainThread);
        this.mContext = context;
        this.mView = view;
    }

    @Override
    public void submitEmail(String email) {
        mInteractor = new ForgetPasswordInteractorImpl(mExecutor, mMainThread, this, new UserRepositoryImpl(), email);
        mInteractor.execute();
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void stop() {

    }

    @Override
    public void destroy() {

    }

    @Override
    public void onForgetPasswordSuccess(String successMsg) {
        mView.hideLoader();
        Toasty.success(mContext, successMsg, Toast.LENGTH_SHORT).show();
        mView.onSuccess();
    }

    @Override
    public void onForgetPasswordFail(String errorMsg) {
        mView.hideLoader();
        Toasty.error(mContext, errorMsg, Toast.LENGTH_SHORT).show();
    }
}
