package com.webinfotech.santirekha2.presentation.presenters;

import com.webinfotech.santirekha2.presentation.ui.adapters.StarProductsAdapter;

public interface StarProductsPresenter {
    void fetchStarProducts(int pageNo, String type);
    interface View {
        void loadData(StarProductsAdapter starProductsAdapter, int totalPage);
        void showLoader();
        void hideLoader();
    }
}
