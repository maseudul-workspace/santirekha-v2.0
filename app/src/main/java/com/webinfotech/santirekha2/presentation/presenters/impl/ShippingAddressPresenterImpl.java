package com.webinfotech.santirekha2.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import com.webinfotech.santirekha2.AndroidApplication;
import com.webinfotech.santirekha2.domain.executors.Executor;
import com.webinfotech.santirekha2.domain.executors.MainThread;
import com.webinfotech.santirekha2.domain.interactors.DeleteShippingAddressInteractor;
import com.webinfotech.santirekha2.domain.interactors.FetchShippingAddressInteractor;
import com.webinfotech.santirekha2.domain.interactors.impl.DeleteShippingAddressInteractorImpl;
import com.webinfotech.santirekha2.domain.interactors.impl.FetchShippingAddressInteractorImpl;
import com.webinfotech.santirekha2.domain.models.User.ShippingAddress;
import com.webinfotech.santirekha2.domain.models.User.UserInfo;
import com.webinfotech.santirekha2.presentation.presenters.ShippingAddressPresenter;
import com.webinfotech.santirekha2.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.santirekha2.presentation.ui.adapters.ShippingAddressAdapter;
import com.webinfotech.santirekha2.repository.User.UserRepositoryImpl;

import es.dmoral.toasty.Toasty;

public class ShippingAddressPresenterImpl extends AbstractPresenter implements ShippingAddressPresenter, FetchShippingAddressInteractor.Callback, ShippingAddressAdapter.Callback, DeleteShippingAddressInteractor.Callback {

    Context mContext;
    ShippingAddressPresenter.View mView;
    FetchShippingAddressInteractorImpl fetchShippingAddressInteractor;
    AndroidApplication androidApplication;
    DeleteShippingAddressInteractorImpl deleteShippingAddressInteractor;

    public ShippingAddressPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchShippingAddress() {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo != null) {
            fetchShippingAddressInteractor = new FetchShippingAddressInteractorImpl(mExecutor, mMainThread, new UserRepositoryImpl(), this, userInfo.userId, userInfo.apiKey);
            fetchShippingAddressInteractor.execute();
            mView.showLoader();
        } else {
            Toasty.warning(mContext, "Your Session Is Expired!! Please Login Again").show();
        }
    }

    @Override
    public void onAddressFetchSuccess(ShippingAddress[] shippingAddresses) {
        ShippingAddressAdapter addressAdapter = new ShippingAddressAdapter(mContext, shippingAddresses, this);
        mView.setAddressAdapter(addressAdapter);
        mView.hideLoader();
    }

    @Override
    public void onAddressFetchFail(String errorMsg) {
        mView.hideLoader();
        Toasty.error(mContext, errorMsg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRemoveClicked(int id) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo != null) {
            deleteShippingAddressInteractor = new DeleteShippingAddressInteractorImpl(mExecutor, mMainThread, new UserRepositoryImpl(), this, userInfo.userId, userInfo.apiKey, id);
            deleteShippingAddressInteractor.execute();
            mView.showLoader();
        } else {
            Toasty.warning(mContext, "Your Session Is Expired!! Please Login Again").show();
        }
    }

    @Override
    public void onEditClicked(int position) {
        mView.goToEditAddressActivity(position);
    }

    @Override
    public void onAddressDeleteSuccess() {
        Toasty.success(mContext, "Address Deleted Successfully", Toast.LENGTH_SHORT).show();
        fetchShippingAddress();
    }

    @Override
    public void onAddressDeleteFail(String errorMsg) {
        mView.hideLoader();
        Toasty.error(mContext, errorMsg, Toast.LENGTH_SHORT).show();
    }
}
