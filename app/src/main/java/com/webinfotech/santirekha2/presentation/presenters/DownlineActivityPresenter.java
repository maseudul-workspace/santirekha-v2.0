package com.webinfotech.santirekha2.presentation.presenters;

import com.webinfotech.santirekha2.presentation.presenters.base.BasePresenter;
import com.webinfotech.santirekha2.presentation.ui.adapters.DownlinesAdapter;

/**
 * Created by Raj on 27-02-2019.
 */

public interface DownlineActivityPresenter extends BasePresenter {
    void getDownlineList();
    interface View{
        void showProgressBar();
        void hideProgressBar();
        void loadData(DownlinesAdapter adapter);
    }
}
