package com.webinfotech.santirekha2.presentation.ui.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.webinfotech.santirekha2.R;
import com.webinfotech.santirekha2.domain.models.Orders.OrderProducts;
import com.webinfotech.santirekha2.util.GlideHelper;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by Raj on 20-08-2019.
 */

public class OrdersProductsListAdapter extends RecyclerView.Adapter<OrdersProductsListAdapter.ViewHolder> {

    Context mContext;
    OrderProducts[] orderProducts;

    public OrdersProductsListAdapter(Context mContext, OrderProducts[] products) {
        this.mContext = mContext;
        this.orderProducts = products;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.recycler_view_order_list_products, viewGroup, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {
        viewHolder.txtViewProductName.setText(orderProducts[i].product_name);
        viewHolder.txtViewProductPrice.setText("₹ "+ orderProducts[i].order_price);
        GlideHelper.setImageView(mContext, viewHolder.imgViewProductImage, mContext.getResources().getString(R.string.base_url) + "/uploads/product_image/" + orderProducts[i].product_image);
        viewHolder.mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });
        viewHolder.txtViewQuantity.setText(Integer.toString(orderProducts[i].order_quantity));
    }

    @Override
    public int getItemCount() {
        return orderProducts.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_view_product_name)
        TextView txtViewProductName;
        @BindView(R.id.img_view_product_image)
        ImageView imgViewProductImage;
        @BindView(R.id.txt_view_product_price)
        TextView txtViewProductPrice;
        @BindView(R.id.main_layout)
        View mainLayout;
        @BindView(R.id.txt_view_quantity)
        TextView txtViewQuantity;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
