package com.webinfotech.santirekha2.presentation.presenters.impl;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.webinfotech.santirekha2.AndroidApplication;
import com.webinfotech.santirekha2.domain.executors.Executor;
import com.webinfotech.santirekha2.domain.executors.MainThread;
import com.webinfotech.santirekha2.domain.interactors.AddToCartInteractor;
import com.webinfotech.santirekha2.domain.interactors.SearchProductsInteractor;
import com.webinfotech.santirekha2.domain.interactors.impl.AddToCartInteractorImpl;
import com.webinfotech.santirekha2.domain.interactors.impl.SearchProductsInteractorImpl;
import com.webinfotech.santirekha2.domain.models.Products.Product;
import com.webinfotech.santirekha2.domain.models.User.UserInfo;
import com.webinfotech.santirekha2.presentation.presenters.SearchActivityPresenter;
import com.webinfotech.santirekha2.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.santirekha2.presentation.ui.adapters.ProductListVerticalAdapter;
import com.webinfotech.santirekha2.repository.ProductRepository.impl.CartRepositoryImpl;
import com.webinfotech.santirekha2.repository.ProductRepository.impl.GetProductsRepositoryImpl;

import es.dmoral.toasty.Toasty;

/**
 * Created by Raj on 27-02-2019.
 */

public class SearchActivityPresenterImpl extends AbstractPresenter implements   SearchActivityPresenter,
                                                                                SearchProductsInteractor.Callback,
                                                                                ProductListVerticalAdapter.Callback,
                                                                                AddToCartInteractor.Callback

{

    Context mContext;
    SearchActivityPresenter.View mView;
    SearchProductsInteractorImpl mInteractor;
    ProductListVerticalAdapter adapter;
    Product[] newProducts;
    String currentSearchKey;
    AndroidApplication androidApplication;
    UserInfo userInfo;
    AddToCartInteractorImpl addToCartInteractor;

    public SearchActivityPresenterImpl(Executor executor,
                                       MainThread mainThread,
                                       Context context,
                                       SearchActivityPresenter.View view) {
        super(executor, mainThread);
        mContext = context;
        mView = view;
    }

    @Override
    public void getProductList(String search_key, int page_no, String type) {
        currentSearchKey = search_key;
        if(type.equals("refresh")){
            newProducts = null;
        }
        mInteractor = new SearchProductsInteractorImpl(mExecutor, mMainThread, this, new GetProductsRepositoryImpl(), search_key, page_no);
        mInteractor.execute();
    }

    @Override
    public void onGettingProductsSuccess(Product[] products, String searchKey, int totalPage) {
        if(currentSearchKey.equals(searchKey)) {
            if(products.length == 0){
                adapter = new ProductListVerticalAdapter(mContext, products, this);
                mView.loadRecyclerViewAdapter(adapter, totalPage);
            }else{
                Product[] tempProducts;
                tempProducts = newProducts;
                try {
                    int len1 = tempProducts.length;
                    int len2 = products.length;
                    newProducts = new Product[len1 + len2];
                    System.arraycopy(tempProducts, 0, newProducts, 0, len1);
                    System.arraycopy(products, 0, newProducts, len1, len2);
                    adapter.updateDataset(newProducts);
                } catch (NullPointerException e) {
                    newProducts = products;
                    adapter = new ProductListVerticalAdapter(mContext, products, this);
                    mView.loadRecyclerViewAdapter(adapter, totalPage);
                }
            }
        }
    }


    @Override
    public void onGettingProductsFail(String searchKey) {
        newProducts = null;
        mView.hideProgressBar();
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void stop() {

    }

    @Override
    public void destroy() {

    }

    @Override
    public void onProductClicked(int productId) {
        mView.goToProductDetails(productId);
    }

    @Override
    public void onAddToCartClicked(int productId) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo != null) {
            addToCartInteractor = new AddToCartInteractorImpl(mExecutor, mMainThread, new CartRepositoryImpl(), this, userInfo.apiKey, productId, userInfo.userId, 1);
            addToCartInteractor.execute();
            mView.showProgressBar();
        } else {
            mView.showLoginSnackBar();
        }
    }

    @Override
    public void onAddToCartSuccess(String successMsg) {
        mView.hideProgressBar();
        mView.showCartSnackbar();
    }

    @Override
    public void onAddToCartFail(String errorMsg) {
        mView.hideProgressBar();
        Toasty.error(mContext, errorMsg, Toast.LENGTH_SHORT).show();
    }
}
