package com.webinfotech.santirekha2.presentation.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.webinfotech.santirekha2.R;
import com.webinfotech.santirekha2.domain.models.Category.SubCategory;
import com.webinfotech.santirekha2.domain.models.Testing.Subcategory;
import com.webinfotech.santirekha2.util.GlideHelper;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class SubcategoryAdapter extends RecyclerView.Adapter<SubcategoryAdapter.ViewHolder> {

    public interface Callback {
        void onSubcategoryClicked(int subcategoryId, String categoryName);
    }

    Context mContext;
    SubCategory[] subcategories;
    Callback mCallback;

    public SubcategoryAdapter(Context mContext, SubCategory[] subcategories, Callback callback) {
        this.mContext = mContext;
        this.subcategories = subcategories;
        mCallback = callback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_sub_category, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        GlideHelper.setImageViewCustomRoundedCorners(mContext, holder.imgViewCategory, mContext.getResources().getString(R.string.base_url) + "/uploads/sub_category/thumb/" + subcategories[position].image, 20);
        holder.txtViewCategoryName.setText(subcategories[position].name);
        holder.mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onSubcategoryClicked(subcategories[position].id, subcategories[position].name);
            }
        });
    }

    @Override
    public int getItemCount() {
        return subcategories.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.img_view_sub_category)
        ImageView imgViewCategory;
        @BindView(R.id.txt_view_subcategory)
        TextView txtViewCategoryName;
        @BindView(R.id.main_layout)
        View mainLayout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }


}
