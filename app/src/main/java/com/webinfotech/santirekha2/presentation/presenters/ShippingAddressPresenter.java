package com.webinfotech.santirekha2.presentation.presenters;

import com.webinfotech.santirekha2.presentation.ui.adapters.ShippingAddressAdapter;

public interface ShippingAddressPresenter {
    void fetchShippingAddress();
    interface View {
        void setAddressAdapter(ShippingAddressAdapter addressAdapter);
        void goToEditAddressActivity(int position);
        void showLoader();
        void hideLoader();
    }
}
