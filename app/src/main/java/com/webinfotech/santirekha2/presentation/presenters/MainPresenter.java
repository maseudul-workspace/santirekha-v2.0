package com.webinfotech.santirekha2.presentation.presenters;

import com.webinfotech.santirekha2.domain.models.ImageSliders.ImageSlider;
import com.webinfotech.santirekha2.presentation.ui.adapters.HomeSlidingImagesAdapter;
import com.webinfotech.santirekha2.presentation.ui.adapters.MainCategoriesAdapter;
import com.webinfotech.santirekha2.presentation.ui.adapters.ProductHorizontalAdapter;
import com.webinfotech.santirekha2.presentation.ui.adapters.ReviewsAdapter;

public interface MainPresenter {
    void getCategories();
    void getMainData();
    void fetchCartCount();
    void fetchStarProductsCount();
    void fetchWalletAmount();
    void submitReview(int rating, String comments);
    void fetchReviews();
    interface View {
        void loadCategoriesAdapter(MainCategoriesAdapter adapter);
        void goToSubcategoryActivity(int catId, String categoryName);
        void goToProductList(int categoryId, String categoryName);
        void loadMainData(ProductHorizontalAdapter newProductsAdapter, ProductHorizontalAdapter popularProductsAdapter, ImageSlider[] imageSliders);
        void showLoader();
        void hideLoader();
        void goToProductDetails(int productId);
        void loadCartCount(int count);
        void loadStarProductsCount(int count);
        void loadWalletAmount(double walletAmount);
        void loadReviews(ReviewsAdapter adapter);
    }
}
