package com.webinfotech.santirekha2.presentation.ui.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Paint;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.webinfotech.santirekha2.R;
import com.webinfotech.santirekha2.domain.models.Cart.Cart;
import com.webinfotech.santirekha2.domain.models.Testing.Products;
import com.webinfotech.santirekha2.util.GlideHelper;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class CartAdapter extends RecyclerView.Adapter<CartAdapter.ViewHolder> {

    public interface Callback {
        void updateCart(int cartId, int quantity);
        void deleteCart(int cartId);
    }

    Context mContext;
    Cart[] carts;
    Callback mCallback;

    public CartAdapter(Context mContext, Cart[] carts, Callback callback) {
        this.mContext = mContext;
        this.carts = carts;
        mCallback = callback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_cart_product_list, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        GlideHelper.setImageView(mContext, holder.imgViewProduct, mContext.getResources().getString(R.string.base_url) + "/uploads/product_image/" + carts[position].image);
        holder.txtViewProductPrice.setText("₹ " + carts[position].price);
        holder.txtViewProductMrp.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);
        holder.txtViewProductMrp.setText("₹ " + carts[position].mrp);
        holder.txtViewQuantity.setText(Integer.toString(carts[position].cartQty));
        holder.txtViewUpdateCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.updateCart(carts[position].cartId, carts[position].cartQty);
            }
        });
        holder.layoutPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (carts[position].cartQty < carts[position].stock) {
                    carts[position].cartQty = carts[position].cartQty + 1;
                    holder.txtViewQuantity.setText(Integer.toString(carts[position].cartQty));
                }
            }
        });
        holder.layoutMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (carts[position].cartQty > 1) {
                    carts[position].cartQty = carts[position].cartQty - 1;
                    holder.txtViewQuantity.setText(Integer.toString(carts[position].cartQty));
                }
            }
        });
        holder.txtViewDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                builder.setTitle("Are You Sure ?");
                builder.setMessage("You are about to delete a remove an item from your cart. Do you really want to proceed ?");
                builder.setCancelable(false);
                builder.setPositiveButton(Html.fromHtml("<font color='#2ac5b3'>Yes</font>"), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mCallback.deleteCart(carts[position].cartId);
                    }
                });
                builder.setNegativeButton(Html.fromHtml("<font color='#2ac5b3'>No</font>"), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });

                builder.show();
            }
        });
        holder.txtViewProductName.setText(carts[position].productName);
        if (carts[position].stock < 1) {
            holder.layoutQty.setVisibility(View.GONE);
            holder.txtViewUpdateCart.setVisibility(View.GONE);
            holder.layoutPlus.setVisibility(View.GONE);
            holder.layoutMinus.setVisibility(View.GONE);
            holder.layoutOfStock.setVisibility(View.VISIBLE);
            holder.txtViewOutOfStock.setVisibility(View.VISIBLE);
        } else {
            holder.layoutQty.setVisibility(View.VISIBLE);
            holder.txtViewUpdateCart.setVisibility(View.VISIBLE);
            holder.layoutPlus.setVisibility(View.VISIBLE);
            holder.layoutMinus.setVisibility(View.VISIBLE);
            holder.layoutOfStock.setVisibility(View.GONE);
            holder.txtViewOutOfStock.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return carts.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.img_view_product)
        ImageView imgViewProduct;
        @BindView(R.id.txt_view_product_price)
        TextView txtViewProductPrice;
        @BindView(R.id.txt_view_quantity)
        TextView txtViewQuantity;
        @BindView(R.id.layout_plus)
        View layoutPlus;
        @BindView(R.id.layout_minus)
        View layoutMinus;
        @BindView(R.id.txt_view_product_mrp)
        TextView txtViewProductMrp;
        @BindView(R.id.txt_view_update_cart)
        TextView txtViewUpdateCart;
        @BindView(R.id.txt_view_delete)
        TextView txtViewDelete;
        @BindView(R.id.txt_view_product_name)
        TextView txtViewProductName;
        @BindView(R.id.txt_view_out_of_stock)
        TextView txtViewOutOfStock;
        @BindView(R.id.layout_quantity)
        View layoutQty;
        @BindView(R.id.layout_out_of_stock)
        View layoutOfStock;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }

}
