package com.webinfotech.santirekha2.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.MenuItem;

import com.webinfotech.santirekha2.R;
import com.webinfotech.santirekha2.domain.executors.impl.ThreadExecutor;
import com.webinfotech.santirekha2.presentation.presenters.OrderHistoryPresenter;
import com.webinfotech.santirekha2.presentation.presenters.impl.OrderHistoryPresenterImpl;
import com.webinfotech.santirekha2.presentation.ui.adapters.OrdersAdapter;
import com.webinfotech.santirekha2.threading.MainThreadImpl;

public class OrderHistoryActivity extends AppCompatActivity implements OrderHistoryPresenter.View {

    @BindView(R.id.recycler_view_orders)
    RecyclerView recyclerViewOrders;
    OrderHistoryPresenterImpl mPresenter;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_history);
        getSupportActionBar().setTitle("My Orders");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ButterKnife.bind(this);
        setProgressDialog();
        initialisePresenter();
        mPresenter.getOrderHistory();
    }

    private void initialisePresenter() {
        mPresenter = new OrderHistoryPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    private void setProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    @Override
    public void loadOrdersAdapter(OrdersAdapter adapter) {
        recyclerViewOrders.setAdapter(adapter);
        recyclerViewOrders.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    public void showProgressBar() {
        progressDialog.show();
    }

    @Override
    public void hideProgressBar() {
        progressDialog.dismiss();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
