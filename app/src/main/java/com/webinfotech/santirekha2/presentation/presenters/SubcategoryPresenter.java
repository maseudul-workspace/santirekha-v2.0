package com.webinfotech.santirekha2.presentation.presenters;

import com.webinfotech.santirekha2.presentation.ui.adapters.SubcategoryAdapter;

public interface SubcategoryPresenter {
    void fetchSubcategories(int catId);
    interface View {
        void loadSubcategoriesAdapter(SubcategoryAdapter subcategoryAdapter);
        void goToProductList(int subcategoryId, String subcategoryName);
        void showLoader();
        void hideLoader();
    }
}
