package com.webinfotech.santirekha2.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import com.webinfotech.santirekha2.R;
import com.webinfotech.santirekha2.domain.executors.impl.ThreadExecutor;
import com.webinfotech.santirekha2.presentation.presenters.RegisterPresenter;
import com.webinfotech.santirekha2.presentation.presenters.impl.RegisterPresenterImpl;
import com.webinfotech.santirekha2.threading.MainThreadImpl;

public class RegisterActivity extends AppCompatActivity implements RegisterPresenter.View {

    @BindView(R.id.edit_text_address)
    EditText editTextAddress;
    @BindView(R.id.edit_text_city)
    EditText editTextCity;
    @BindView(R.id.edit_text_email)
    EditText editTextEmail;
    @BindView(R.id.edit_text_name)
    EditText editTextName;
    @BindView(R.id.edit_text_phone)
    EditText editTextPhone;
    @BindView(R.id.edit_text_pin)
    EditText editTextPin;
    @BindView(R.id.edit_text_state)
    EditText editTextState;
    @BindView(R.id.edit_text_password)
    EditText editTextPassword;
    @BindView(R.id.edit_text_confirm_password)
    EditText editTextConfirmPassword;
    @BindView(R.id.edit_text_refer_code)
    EditText editTextReferCode;
    RegisterPresenterImpl mPresenter;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);
        setUpProgressDialog();
        getSupportActionBar().setTitle("Register");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        initialisePresenter();
    }

    private void initialisePresenter() {
        mPresenter = new RegisterPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    private void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    @OnClick(R.id.btn_register) void onRegisterClicked() {
        if (   editTextName.getText().toString().trim().isEmpty() ||
               editTextEmail.getText().toString().trim().isEmpty() ||
               editTextPhone.getText().toString().trim().isEmpty() ||
               editTextPin.getText().toString().trim().isEmpty() ||
               editTextState.getText().toString().trim().isEmpty() ||
               editTextAddress.getText().toString().trim().isEmpty() ||
               editTextCity.getText().toString().trim().isEmpty() ||
               editTextPassword.getText().toString().trim().isEmpty() ||
               editTextConfirmPassword.getText().toString().isEmpty()
        ) {
            Toasty.warning(this, "Some Fields Are Empty", Toast.LENGTH_LONG).show();
        } else if (!editTextPassword.getText().toString().trim().equals(editTextConfirmPassword.getText().toString().trim())) {
            Toasty.warning(this, "Password Mismatch", Toast.LENGTH_LONG).show();
        } else {
            mPresenter.createUser(
                    editTextName.getText().toString(),
                    editTextPhone.getText().toString(),
                    editTextEmail.getText().toString(),
                    editTextPassword.getText().toString(),
                    editTextState.getText().toString(),
                    editTextCity.getText().toString(),
                    editTextAddress.getText().toString(),
                    editTextReferCode.getText().toString(),
                    editTextPin.getText().toString()
            );
            showLoader();
        }
    }

    @Override
    public void onSignUpSuccess() {
        editTextName.setText("");
        editTextPassword.setText("");
        editTextConfirmPassword.setText("");
        editTextCity.setText("");
        editTextAddress.setText("");
        editTextState.setText("");
        editTextPin.setText("");
        editTextPhone.setText("");
        editTextEmail.setText("");
        editTextReferCode.setText("");
        finish();
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


}
