package com.webinfotech.santirekha2.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ImageView;

import com.google.android.material.snackbar.Snackbar;
import com.webinfotech.santirekha2.R;
import com.webinfotech.santirekha2.domain.executors.impl.ThreadExecutor;
import com.webinfotech.santirekha2.domain.models.Testing.Products;
import com.webinfotech.santirekha2.presentation.presenters.ProductListPresenter;
import com.webinfotech.santirekha2.presentation.presenters.impl.ProductListPresenterImpl;
import com.webinfotech.santirekha2.presentation.ui.adapters.ProductListVerticalAdapter;
import com.webinfotech.santirekha2.threading.MainThreadImpl;

import java.util.ArrayList;

public class ProductListActivity extends AppCompatActivity implements ProductListPresenter.View {

    @BindView(R.id.recycler_view_product_list_vertical)
    RecyclerView recyclerViewProductList;
    @BindView(R.id.main_layout)
    View constraintLayout;
    @BindView(R.id.img_view_no_product)
    ImageView imgViewNoProduct;
    ProductListPresenterImpl mPresenter;
    ProgressDialog progressDialog;
    Boolean isScrolling = false;
    Integer currentItems;
    Integer totalItems;
    Integer scrollOutItems;
    int pageNo = 1;
    int totalPage = 1;
    int categoryId;
    LinearLayoutManager layoutManager;
    int type;
    String categoryName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_list);
        categoryId = getIntent().getIntExtra("categoryId", 0);
        type = getIntent().getIntExtra("type", 0);
        categoryName = getIntent().getStringExtra("categoryName");
        getSupportActionBar().setTitle(categoryName);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ButterKnife.bind(this);
        initialisePresenter();
        setUpProgressDialog();
        mPresenter.fetchProductList(type, pageNo, categoryId, "refresh");
        showLoader();
    }

    private void initialisePresenter() {
        mPresenter = new ProductListPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    private void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    @Override
    public void loadProductListAdapter(ProductListVerticalAdapter productListVerticalAdapter, int totalPage) {
        this.totalPage = totalPage;
        recyclerViewProductList.setVisibility(View.VISIBLE);
        imgViewNoProduct.setVisibility(View.GONE);
        layoutManager = new GridLayoutManager(this,2);
        recyclerViewProductList.setLayoutManager(layoutManager);
        recyclerViewProductList.setAdapter(productListVerticalAdapter);
        recyclerViewProductList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if(newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL)
                {
                    isScrolling= true;
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                currentItems = layoutManager.getChildCount();
                totalItems  = layoutManager.getItemCount();
                scrollOutItems = layoutManager.findFirstCompletelyVisibleItemPosition();
                if(!recyclerView.canScrollVertically(1))
                {
                    if(pageNo < totalPage) {
                        isScrolling = false;
                        pageNo = pageNo + 1;
                        mPresenter.fetchProductList(2, pageNo, categoryId, "");
                    }
                }
            }
        });
    }

    @Override
    public void goToProductDetails(int productId) {
        Intent intent = new Intent(this, ProductDetailsActivity.class);
        intent.putExtra("productId", productId);
        startActivity(intent);
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    @Override
    public void showLoginSnackbar() {
        Snackbar snackbar = Snackbar.make(constraintLayout,"You must be logged in",Snackbar.LENGTH_LONG);
        snackbar.setAction("Log In", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent cartIntent = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(cartIntent);
            }
        });
        snackbar.show();
    }

    @Override
    public void showGoToCartSnackbar() {
        Snackbar snackbar = Snackbar.make(constraintLayout,"Added To Cart",Snackbar.LENGTH_LONG);
        snackbar.setAction("Go To Cart", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent cartIntent = new Intent(getApplicationContext(), CartListActivity.class);
                startActivity(cartIntent);
            }
        });
        snackbar.show();
    }

    @Override
    public void onNoProductsFound() {
        imgViewNoProduct.setVisibility(View.VISIBLE);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


}
