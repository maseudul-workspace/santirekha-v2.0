package com.webinfotech.santirekha2.presentation.presenters;

import com.webinfotech.santirekha2.presentation.presenters.base.BasePresenter;

/**
 * Created by Raj on 14-02-2019.
 */

public interface ChangePasswordPresenter extends BasePresenter {
    void changePassword(String newPassword, String oldPassword);
    interface View{
        void onChangePasswordSuccess();
        void loadProgressBar();
        void hideProgressBar();
    }
}
