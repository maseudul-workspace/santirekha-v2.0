package com.webinfotech.santirekha2.presentation.presenters;

import com.webinfotech.santirekha2.domain.models.Cart.Cart;
import com.webinfotech.santirekha2.presentation.presenters.base.BasePresenter;
import com.webinfotech.santirekha2.presentation.ui.adapters.CartAdapter;
import com.webinfotech.santirekha2.presentation.ui.adapters.CartShippingAddressAdapter;

/**
 * Created by Raj on 10-01-2019.
 */

public interface CartDetailsActivityPresenter extends BasePresenter {
    void getCartList();
    void fetchShippingAddress();
    void fetchWalletAmount();
    void setWalletPayStatus(int walletPayStatus);
    interface View{
        void loadCartItemList(CartAdapter adapter, double subTotal, double discount, double grandTotal);
        void loadAddressAdapter(CartShippingAddressAdapter cartShippingAddressAdapter);
        void loadWalletAmount(double walletAmount);
        void goToOrderHistory();
        void setStockStatus(boolean isOutOfStock);
        void hideViews();
        void showLoadingProgress();
        void hideLoadingProgress();
    }
}
