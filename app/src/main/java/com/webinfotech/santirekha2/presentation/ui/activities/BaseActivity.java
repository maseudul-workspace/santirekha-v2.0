package com.webinfotech.santirekha2.presentation.ui.activities;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.google.android.material.navigation.NavigationView;
import com.webinfotech.santirekha2.AndroidApplication;
import com.webinfotech.santirekha2.R;
import com.webinfotech.santirekha2.domain.models.User.Downline;
import com.webinfotech.santirekha2.domain.models.User.UserInfo;

public class BaseActivity extends AppCompatActivity {

    @BindView(R.id.toolbar) @Nullable
    Toolbar toolbar;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;
    ActionBarDrawerToggle actionBarDrawerToggle;
    @BindView(R.id.navigation)
    NavigationView navigationView;
    AndroidApplication androidApplication;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);
        getUserInfo();
    }

    protected void inflateContent(@LayoutRes int inflateResID){
        setContentView(R.layout.activity_base);
        FrameLayout contentFramelayout = (FrameLayout) findViewById(R.id.content_frame);
        getLayoutInflater().inflate(inflateResID, contentFramelayout);
        ButterKnife.bind(this);
        setToolbar();
        setUpNavigationView();
    }

    public void setToolbar() {
        setSupportActionBar(toolbar);
        actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.app_name, R.string.app_name);
        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.menu);
        getSupportActionBar().setTitle("");
    }

    public void setUpNavigationView() {

        Menu navMenu = navigationView.getMenu();

        if (checkLogin()) {
            navMenu.findItem(R.id.nav_log_in).setVisible(false);
            navMenu.findItem(R.id.nav_log_out).setVisible(true);
            navMenu.findItem(R.id.nav_address).setVisible(true);
            navMenu.findItem(R.id.nav_change_password).setVisible(true);
            navMenu.findItem(R.id.nav_downlines).setVisible(true);
            navMenu.findItem(R.id.nav_orders).setVisible(true);
            navMenu.findItem(R.id.nav_wallet).setVisible(true);
            navMenu.findItem(R.id.nav_credit_history).setVisible(true);
        } else {
            navMenu.findItem(R.id.nav_log_in).setVisible(true);
            navMenu.findItem(R.id.nav_log_out).setVisible(false);
            navMenu.findItem(R.id.nav_address).setVisible(false);
            navMenu.findItem(R.id.nav_change_password).setVisible(false);
            navMenu.findItem(R.id.nav_downlines).setVisible(false);
            navMenu.findItem(R.id.nav_orders).setVisible(false);
            navMenu.findItem(R.id.nav_wallet).setVisible(false);
            navMenu.findItem(R.id.nav_credit_history).setVisible(false);
        }

        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                int id = item.getItemId();
                switch (id) {
                    case R.id.nav_log_in:
                        goToLogin();
                        break;
                    case R.id.nav_address:
                        goToShippingAddress();
                        break;
                    case R.id.nav_change_password:
                        goToChangePasswordActivity();
                        break;
                    case R.id.nav_orders:
                        goToMyOrders();
                        break;
                    case R.id.nav_wallet:
                        goToWalletHistory();
                        break;
                    case R.id.nav_credit_history:
                        goToCreditHistory();
                        break;
                    case R.id.nav_downlines:
                        goToDownlinesActivity();
                        break;
                    case R.id.nav_log_out:
                        androidApplication = (AndroidApplication) getApplicationContext();
                        androidApplication.setUserInfo(getApplicationContext(), null);
                        Toasty.success(getApplicationContext(), "Successfully Logged Out", Toast.LENGTH_SHORT).show();
                        goToMainActivity();
                        break;
                    case R.id.nav_about_us:
                        goToAboutUsActivity();
                        break;
                    case R.id.nav_contact_us:
                        goToContactUsActivity();
                        break;
                    case R.id.nav_delivery_schedule:
                        goToDeliveryScheduleActivity();
                        break;
                }
                return false;
            }
        });
    }

    @OnClick(R.id.img_view_cart) void onCartClicked() {
        if (checkLogin()) {
            Intent intent = new Intent(this, CartListActivity.class);
            startActivity(intent);
        } else {
            Toasty.warning(this, "Please Login", Toast.LENGTH_SHORT).show();
        }
    }

    @OnClick(R.id.txt_view_order_list) void onOrderListClicked() {
        if (checkLogin()) {
            goToMyOrders();
        } else {
            Toasty.warning(this, "Please Login", Toast.LENGTH_SHORT).show();
        }
    }

    @OnClick(R.id.search_layout) void onSearchClicked() {
        Intent intent = new Intent(this, SearchActivity.class);
        startActivity(intent);
    }

    private void goToRegister() {
        Intent intent = new Intent(this, RegisterActivity.class);
        startActivity(intent);
    }

    private void goToLogin() {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
    }

    private void getUserInfo() {
        AndroidApplication androidApplication = (AndroidApplication) getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(this);
        if (userInfo != null) {
            Log.e("LogMsg", "Api Key: " + userInfo.apiKey);
            Log.e("LogMsg", "User Id: " + userInfo.userId);
        }
    }

    private void goToShippingAddress() {
        Intent intent = new Intent(this, ShippingAddressActivity.class);
        startActivity(intent);
    }

    private void goToChangePasswordActivity() {
        Intent intent = new Intent(this, ChangePasswordActivity.class);
        startActivity(intent);
    }

    private void goToMyOrders() {
        Intent intent = new Intent(this, OrderHistoryActivity.class);
        startActivity(intent);
    }

    private void goToWalletHistory() {
        Intent intent = new Intent(this, WalletHistoryActivity.class);
        startActivity(intent);
    }

    private void goToDownlinesActivity() {
        Intent intent = new Intent(this, DownlinesActivity.class);
        startActivity(intent);
    }

    private boolean checkLogin() {
        androidApplication = (AndroidApplication) getApplicationContext();
        UserInfo user = androidApplication.getUserInfo(this);
        if (user == null) {
            return false;
        } else {
            return true;
        }
    }

    private void goToMainActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    private void goToAboutUsActivity() {
        Intent intent = new Intent(this, AboutUsActivity.class);
        startActivity(intent);
    }

    private void goToContactUsActivity() {
        Intent intent = new Intent(this, ContactUsActivity.class);
        startActivity(intent);
    }

    private void goToDeliveryScheduleActivity() {
        Intent intent = new Intent(this, DeliveryScheduleActivity.class);
        startActivity(intent);
    }

    private void goToCreditHistory() {
        Intent intent = new Intent(this, CreditHistoryActivity.class);
        startActivity(intent);
    }

}
