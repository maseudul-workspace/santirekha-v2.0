package com.webinfotech.santirekha2.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import com.webinfotech.santirekha2.AndroidApplication;
import com.webinfotech.santirekha2.domain.executors.Executor;
import com.webinfotech.santirekha2.domain.executors.MainThread;
import com.webinfotech.santirekha2.domain.interactors.FetchShippingAddressInteractor;
import com.webinfotech.santirekha2.domain.interactors.UpdateShippingAddressInteractor;
import com.webinfotech.santirekha2.domain.interactors.impl.FetchShippingAddressInteractorImpl;
import com.webinfotech.santirekha2.domain.interactors.impl.UpdateShippingAddressInteractorImpl;
import com.webinfotech.santirekha2.domain.models.User.ShippingAddress;
import com.webinfotech.santirekha2.domain.models.User.UserInfo;
import com.webinfotech.santirekha2.presentation.presenters.EditAddressPresenter;
import com.webinfotech.santirekha2.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.santirekha2.repository.User.UserRepositoryImpl;

import es.dmoral.toasty.Toasty;

public class EditAddressPresenterImpl extends AbstractPresenter implements EditAddressPresenter, FetchShippingAddressInteractor.Callback, UpdateShippingAddressInteractor.Callback {

    Context mContext;
    EditAddressPresenter.View mView;
    FetchShippingAddressInteractorImpl fetchShippingAddressInteractor;
    AndroidApplication androidApplication;
    int position;
    UpdateShippingAddressInteractorImpl updateShippingAddressInteractor;

    public EditAddressPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchShippingAddress(int position) {
        this.position = position;
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo != null) {
            fetchShippingAddressInteractor = new FetchShippingAddressInteractorImpl(mExecutor, mMainThread, new UserRepositoryImpl(), this, userInfo.userId, userInfo.apiKey);
            fetchShippingAddressInteractor.execute();
            mView.showLoader();
        } else {
            Toasty.warning(mContext, "Your Session Is Expired!! Please Login Again").show();
        }
    }

    @Override
    public void updateShippingAddress(int addressId, String mobile, String email, String state, String city, String address, String pin) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo != null) {
            updateShippingAddressInteractor = new UpdateShippingAddressInteractorImpl(mExecutor, mMainThread, new UserRepositoryImpl(), this, userInfo.userId, userInfo.apiKey, addressId, mobile, email, state, city, address, pin);
            updateShippingAddressInteractor.execute();
            mView.showLoader();
        } else {
            Toasty.warning(mContext, "Your Session Is Expired!! Please Login Again").show();
        }
    }

    @Override
    public void onAddressFetchSuccess(ShippingAddress[] shippingAddresses) {
        mView.loadAddress(shippingAddresses[position]);
        mView.hideLoader();
    }

    @Override
    public void onAddressFetchFail(String errorMsg) {
        mView.hideLoader();
    }

    @Override
    public void onEditAddressSuccess() {
        mView.hideLoader();
        Toasty.success(mContext, "Address Updated Successfully", Toast.LENGTH_SHORT).show();
        mView.onAddressEditSuccess();
    }

    @Override
    public void onEditAddressFail(String errorMsg) {
        mView.hideLoader();
        Toasty.error(mContext, errorMsg, Toast.LENGTH_SHORT).show();
    }
}
