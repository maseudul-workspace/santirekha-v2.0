package com.webinfotech.santirekha2.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.webinfotech.santirekha2.AndroidApplication;
import com.webinfotech.santirekha2.R;
import com.webinfotech.santirekha2.domain.executors.impl.ThreadExecutor;
import com.webinfotech.santirekha2.domain.models.Testing.Products;
import com.webinfotech.santirekha2.domain.models.User.UserInfo;
import com.webinfotech.santirekha2.presentation.presenters.CartDetailsActivityPresenter;
import com.webinfotech.santirekha2.presentation.presenters.impl.CartDetailsActivityPresenterImpl;
import com.webinfotech.santirekha2.presentation.ui.adapters.CartAdapter;
import com.webinfotech.santirekha2.presentation.ui.adapters.CartShippingAddressAdapter;
import com.webinfotech.santirekha2.threading.MainThreadImpl;

import java.util.ArrayList;

import static androidx.recyclerview.widget.RecyclerView.HORIZONTAL;

public class CartListActivity extends AppCompatActivity implements CartDetailsActivityPresenter.View {

    @BindView(R.id.recycler_view_cart_list)
    RecyclerView recyclerViewCartList;
    CartDetailsActivityPresenterImpl mPresenter;
    ProgressDialog progressDialog;
    @BindView(R.id.layout_summary)
    View layoutSummary;
    @BindView(R.id.txt_view_sub_total)
    TextView txtViewSubTotal;
    @BindView(R.id.txt_view_discount)
    TextView txtViewDiscount;
    @BindView(R.id.txt_view_grand_total)
    TextView txtViewGrandTotal;
    @BindView(R.id.btn_delivery_address)
    Button btnDeliveryAddress;
    BottomSheetDialog addressBottomSheetDialog;
    TextView txtViewAddAddress;
    ImageView imgViewAddAddress;
    RecyclerView recyclerViewShippingAddress;
    @BindView(R.id.wallet_check_box)
    CheckBox walletCheckbox;
    double grandTotal;
    double walletAmount;
    @BindView(R.id.txt_view_wallet_amount)
    TextView txtViewWalletAmount;
    @BindView(R.id.layout_wallet_pay)
    View walletPay;
    boolean isOutOfStock = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart_list);
        ButterKnife.bind(this);
        getSupportActionBar().setTitle("Cart");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setAddressBottomSheetDialog();
        setWalletCheckboxListener();
        initialisePresenter();
        setUpProgressDialog();
        mPresenter.getCartList();
        mPresenter.fetchWalletAmount();
    }

    private void initialisePresenter() {
        mPresenter = new CartDetailsActivityPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    private void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    private void setAddressBottomSheetDialog() {
        if (addressBottomSheetDialog == null) {
            View view = LayoutInflater.from(this).inflate(R.layout.layout_address_bottom_sheet, null);
            addressBottomSheetDialog = new BottomSheetDialog(this);
            txtViewAddAddress = (TextView) view.findViewById(R.id.txt_view_add_address);
            imgViewAddAddress = (ImageView) view.findViewById(R.id.img_view_add_address);
            recyclerViewShippingAddress = (RecyclerView) view.findViewById(R.id.recycler_view_address);
            txtViewAddAddress.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getApplicationContext(), AddAddressActivity.class);
                    startActivity(intent);
                }
            });
            addressBottomSheetDialog.setContentView(view);
        }
    }

    private void setWalletCheckboxListener() {
        walletCheckbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    if (grandTotal >= walletAmount) {
                        double net = grandTotal - walletAmount;
                        txtViewGrandTotal.setText("₹ " + net);
                        walletCheckbox.setText("Wallet Pay(₹ 0.0)");
                        txtViewWalletAmount.setText("₹ " + walletAmount);
                    } else {
                        txtViewGrandTotal.setText("₹ 0.0");
                        walletCheckbox.setText("Wallet Pay(₹ " + (walletAmount - grandTotal) + ")");
                        txtViewWalletAmount.setText("₹ " + grandTotal);
                    }
                    mPresenter.setWalletPayStatus(2);
                    walletPay.setVisibility(View.VISIBLE);
                } else {
                    txtViewGrandTotal.setText("₹ " + grandTotal);
                    mPresenter.setWalletPayStatus(1);
                    walletCheckbox.setText("Wallet Pay(₹ " + walletAmount + ")");
                    walletPay.setVisibility(View.GONE);
                }
            }
        });
    }

    @Override
    public void loadCartItemList(CartAdapter adapter, double subTotal, double discount, double grandTotal) {

        this.grandTotal = grandTotal;

        recyclerViewCartList.setLayoutManager(new LinearLayoutManager(this));
        recyclerViewCartList.setAdapter(adapter);
        DividerItemDecoration itemDecor = new DividerItemDecoration(this, DividerItemDecoration.VERTICAL);
        recyclerViewCartList.addItemDecoration(itemDecor);

        if (walletCheckbox.isChecked()) {
            if (grandTotal > walletAmount) {
                double net = grandTotal - walletAmount;
                txtViewGrandTotal.setText("₹ " + net);
            } else {
                txtViewGrandTotal.setText( "₹ 0.0");
            }
        } else {
            txtViewGrandTotal.setText("₹ " + grandTotal);
        }

        layoutSummary.setVisibility(View.VISIBLE);
        txtViewSubTotal.setText("₹ " + subTotal);
        txtViewDiscount.setText("₹ " + discount);

        btnDeliveryAddress.setVisibility(View.VISIBLE);

        walletCheckbox.setVisibility(View.VISIBLE);

    }

    @Override
    public void loadAddressAdapter(CartShippingAddressAdapter cartShippingAddressAdapter) {
        recyclerViewShippingAddress.setAdapter(cartShippingAddressAdapter);
        recyclerViewShippingAddress.setLayoutManager(new LinearLayoutManager(this));
        DividerItemDecoration itemDecor = new DividerItemDecoration(this, DividerItemDecoration.VERTICAL);
        recyclerViewShippingAddress.addItemDecoration(itemDecor);
    }

    @Override
    public void loadWalletAmount(double walletAmount) {
        AndroidApplication androidApplication = (AndroidApplication) getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(this);
        if (userInfo.isStar == 1) {
            walletCheckbox.setText("Wallet Pay(₹ " + walletAmount + ") You must be star member to use wallet");
            walletCheckbox.setEnabled(false);
        } else {
            walletCheckbox.setText("Wallet Pay(₹ " + walletAmount + ")");
        }
        this.walletAmount = walletAmount;
        if (walletAmount < 1) {
            walletCheckbox.setVisibility(View.GONE);
        }
    }

    @Override
    public void goToOrderHistory() {
        Intent intent = new Intent(this, OrderHistoryActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void setStockStatus(boolean isOutOfStock) {
        this.isOutOfStock = isOutOfStock;
    }

    @Override
    public void hideViews() {
        recyclerViewCartList.setVisibility(View.GONE);
        layoutSummary.setVisibility(View.GONE);
        btnDeliveryAddress.setVisibility(View.GONE);
        walletCheckbox.setVisibility(View.GONE);
    }

    @Override
    public void showLoadingProgress() {
        progressDialog.show();
    }

    @Override
    public void hideLoadingProgress() {
        progressDialog.dismiss();
    }

    @OnClick(R.id.btn_delivery_address) void onDeliveryAddressClicked() {
        if (isOutOfStock) {
            Toasty.warning(this, "Some items in your cart is out of stock !!! Please delete them to proceed", Toasty.LENGTH_LONG).show();
        } else {
            addressBottomSheetDialog.show();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPresenter.fetchShippingAddress();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


}
