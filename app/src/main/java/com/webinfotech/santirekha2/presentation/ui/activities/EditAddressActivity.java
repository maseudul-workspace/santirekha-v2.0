package com.webinfotech.santirekha2.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import com.webinfotech.santirekha2.R;
import com.webinfotech.santirekha2.domain.executors.impl.ThreadExecutor;
import com.webinfotech.santirekha2.domain.models.User.ShippingAddress;
import com.webinfotech.santirekha2.presentation.presenters.EditAddressPresenter;
import com.webinfotech.santirekha2.presentation.presenters.impl.AddAddressPresenterImpl;
import com.webinfotech.santirekha2.presentation.presenters.impl.EditAddressPresenterImpl;
import com.webinfotech.santirekha2.threading.MainThreadImpl;

public class EditAddressActivity extends AppCompatActivity implements EditAddressPresenter.View {

    @BindView(R.id.edit_text_email)
    EditText editTextEmail;
    @BindView(R.id.edit_text_phone)
    EditText editTextPhone;
    @BindView(R.id.edit_text_city)
    EditText editTextCity;
    @BindView(R.id.edit_text_address)
    EditText editTextAddress;
    @BindView(R.id.edit_text_pin)
    EditText editTextPin;
    @BindView(R.id.edit_text_state)
    EditText editTextState;
    EditAddressPresenterImpl mPresenter;
    ProgressDialog progressDialog;
    int position;
    int addressId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_address);
        getSupportActionBar().setTitle("Edit Address");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        position = getIntent().getIntExtra("position", 0);
        ButterKnife.bind(this);
        setUpProgressDialog();
        initialisePresenter();
        mPresenter.fetchShippingAddress(position);
    }

    private void initialisePresenter() {
        mPresenter = new EditAddressPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    private void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    @Override
    public void loadAddress(ShippingAddress shippingAddress) {
        addressId = shippingAddress.id;
        editTextAddress.setText(shippingAddress.address);
        editTextCity.setText(shippingAddress.city);
        editTextEmail.setText(shippingAddress.email);
        editTextPhone.setText(shippingAddress.mobile);
        editTextPin.setText(shippingAddress.pin);
        editTextState.setText(shippingAddress.state);
    }

    @Override
    public void onAddressEditSuccess() {
        finish();
    }

    @OnClick(R.id.btn_update) void onUpdateClicked() {
        if (editTextEmail.getText().toString().trim().isEmpty() ||
                editTextPhone.getText().toString().trim().isEmpty() ||
                editTextAddress.getText().toString().trim().isEmpty() ||
                editTextCity.getText().toString().trim().isEmpty() ||
                editTextPin.getText().toString().trim().isEmpty()
        ) {
            Toasty.warning(this, "Some Fields Are Empty", Toast.LENGTH_SHORT).show();
        } else {
            mPresenter.updateShippingAddress(
                    addressId,
                    editTextPhone.getText().toString(),
                    editTextEmail.getText().toString(),
                    editTextState.getText().toString(),
                    editTextCity.getText().toString(),
                    editTextAddress.getText().toString(),
                    editTextPin.getText().toString()
            );
        }
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


}
