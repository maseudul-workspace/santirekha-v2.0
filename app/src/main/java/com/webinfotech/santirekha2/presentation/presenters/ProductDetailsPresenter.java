package com.webinfotech.santirekha2.presentation.presenters;

import com.webinfotech.santirekha2.domain.models.Products.Product;
import com.webinfotech.santirekha2.domain.models.Products.ProductDetailsData;
import com.webinfotech.santirekha2.presentation.ui.adapters.ProductHorizontalAdapter;

public interface ProductDetailsPresenter {
    void fetchProductDetails(int productId);
    void addToCart(int productId, int quantity);
    interface View {
        void loadProductDetails(ProductDetailsData product, ProductHorizontalAdapter adapter);
        void showCartSnackbar();
        void showLoginSnackbar();
        void showLoader();
        void hideLoader();
        void goToProductDetails(int productId);
    }
}
