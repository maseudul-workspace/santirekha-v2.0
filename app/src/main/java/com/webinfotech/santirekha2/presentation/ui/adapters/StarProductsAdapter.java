package com.webinfotech.santirekha2.presentation.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.webinfotech.santirekha2.R;
import com.webinfotech.santirekha2.domain.models.Products.StarProduct;
import com.webinfotech.santirekha2.util.GlideHelper;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class StarProductsAdapter extends RecyclerView.Adapter<StarProductsAdapter.ViewHolder> {

    public interface Callback {
        void onProductClicked(int productId);
    }

    Context mContext;
    StarProduct[] starProducts;
    Callback mCallback;

    public StarProductsAdapter(Context mContext, StarProduct[] starProducts, Callback mCallback) {
        this.mContext = mContext;
        this.starProducts = starProducts;
        this.mCallback = mCallback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_star_products, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        GlideHelper.setImageViewCustomRoundedCorners(mContext, holder.imgViewProduct, mContext.getResources().getString(R.string.base_url) + "/uploads/product_image/thumb/" + starProducts[position].image, 20);
        holder.txtViewProductName.setText(starProducts[position].productName);
        holder.txtViewProductPrice.setText("₹ " + starProducts[position].price);
        holder.mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });
        holder.txtViewPurchaseDate.setText("On: " + starProducts[position].purchaseDate);
    }

    @Override
    public int getItemCount() {
        return starProducts.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.img_view_product)
        ImageView imgViewProduct;
        @BindView(R.id.txt_view_product_name)
        TextView txtViewProductName;
        @BindView(R.id.txt_view_product_price)
        TextView txtViewProductPrice;
        @BindView(R.id.main_layout)
        View mainLayout;
        @BindView(R.id.txt_view_purchase_date)
        TextView txtViewPurchaseDate;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public void updateData(StarProduct[] starProducts) {
        this.starProducts = starProducts;
    }

}
