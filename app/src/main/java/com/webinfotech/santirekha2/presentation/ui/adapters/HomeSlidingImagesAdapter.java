package com.webinfotech.santirekha2.presentation.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.webinfotech.santirekha2.R;
import com.webinfotech.santirekha2.domain.models.ImageSliders.ImageSlider;
import com.webinfotech.santirekha2.domain.models.Testing.HomeSliders;
import com.webinfotech.santirekha2.util.GlideHelper;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

public class HomeSlidingImagesAdapter extends PagerAdapter {

    Context mContext;
    ImageSlider[] imageSliders;

    public HomeSlidingImagesAdapter(Context mContext, ImageSlider[] imageSliders) {
        this.mContext = mContext;
        this.imageSliders = imageSliders;
    }

    @Override
    public int getCount() {
        return imageSliders.length;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        ViewGroup layout = (ViewGroup) inflater.inflate(R.layout.layout_home_slider, container, false);
        ImageView imageView = (ImageView) layout.findViewById(R.id.img_view_home);
        TextView textView = (TextView) layout.findViewById(R.id.txt_view_home_slider);
        textView.setText(imageSliders[position].title);
        GlideHelper.setImageViewCustomRoundedCorners(mContext, imageView, mContext.getResources().getString(R.string.base_url) + "/uploads/slider_image/" + imageSliders[position].image, 20);
        container.addView(layout);
        return layout;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }

}
