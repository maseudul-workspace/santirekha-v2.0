package com.webinfotech.santirekha2.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputLayout;
import com.webinfotech.santirekha2.R;
import com.webinfotech.santirekha2.domain.executors.impl.ThreadExecutor;
import com.webinfotech.santirekha2.presentation.presenters.AddAddressPresenter;
import com.webinfotech.santirekha2.presentation.presenters.impl.AddAddressPresenterImpl;
import com.webinfotech.santirekha2.threading.MainThreadImpl;

public class AddAddressActivity extends AppCompatActivity implements AddAddressPresenter.View {

    @BindView(R.id.edit_text_email)
    EditText editTextEmail;
    @BindView(R.id.edit_text_phone)
    EditText editTextPhone;
    @BindView(R.id.edit_text_city)
    EditText editTextCity;
    @BindView(R.id.edit_text_address)
    EditText editTextAddress;
    @BindView(R.id.edit_text_pin)
    EditText editTextPin;
    @BindView(R.id.edit_text_state)
    EditText editTextState;
    AddAddressPresenterImpl mPresenter;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_address);
        getSupportActionBar().setTitle("Add Address");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ButterKnife.bind(this);
        initialisePresenter();
        setUpProgressDialog();
    }

    private void initialisePresenter() {
        mPresenter = new AddAddressPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    private void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    @OnClick(R.id.btn_submit) void onBtnSubmitClicked() {
        if (editTextEmail.getText().toString().trim().isEmpty() ||
            editTextPhone.getText().toString().trim().isEmpty() ||
            editTextAddress.getText().toString().trim().isEmpty() ||
            editTextCity.getText().toString().trim().isEmpty() ||
            editTextPin.getText().toString().trim().isEmpty()
        ) {
            Toasty.warning(this, "Some Fields Are Empty", Toast.LENGTH_SHORT).show();
        } else {
            mPresenter.addAddress(
                    editTextPhone.getText().toString(),
                    editTextEmail.getText().toString(),
                    editTextState.getText().toString(),
                    editTextCity.getText().toString(),
                    editTextAddress.getText().toString(),
                    editTextPin.getText().toString()
            );
        }
    }

    @Override
    public void onAddressAddSuccess() {
        editTextState.setText("");
        editTextPin.setText("");
        editTextCity.setText("");
        editTextAddress.setText("");
        editTextPhone.setText("");
        editTextEmail.setText("");
        finish();
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


}
