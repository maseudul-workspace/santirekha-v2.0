package com.webinfotech.santirekha2.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;
import butterknife.BindView;
import butterknife.OnClick;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

import com.webinfotech.santirekha2.AndroidApplication;
import com.webinfotech.santirekha2.R;
import com.webinfotech.santirekha2.domain.executors.impl.ThreadExecutor;
import com.webinfotech.santirekha2.domain.models.ImageSliders.ImageSlider;
import com.webinfotech.santirekha2.domain.models.Testing.Brands;
import com.webinfotech.santirekha2.domain.models.Testing.HomeSliders;
import com.webinfotech.santirekha2.domain.models.Testing.MainCategories;
import com.webinfotech.santirekha2.domain.models.Testing.Products;
import com.webinfotech.santirekha2.presentation.presenters.MainPresenter;
import com.webinfotech.santirekha2.presentation.presenters.impl.MainPresenterImpl;
import com.webinfotech.santirekha2.presentation.ui.adapters.BrandsAdapter;
import com.webinfotech.santirekha2.presentation.ui.adapters.HomeSlidingImagesAdapter;
import com.webinfotech.santirekha2.presentation.ui.adapters.MainCategoriesAdapter;
import com.webinfotech.santirekha2.presentation.ui.adapters.ProductHorizontalAdapter;
import com.webinfotech.santirekha2.presentation.ui.adapters.ReviewsAdapter;
import com.webinfotech.santirekha2.threading.MainThreadImpl;
import com.webinfotech.santirekha2.util.GlideHelper;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends BaseActivity implements MainPresenter.View {

    @BindView(R.id.viewpager) ViewPager viewPager;
    @BindView(R.id.dots_layout) LinearLayout dotsLinearLayout;
    @BindView(R.id.recycler_view_categories) RecyclerView recyclerViewCategories;
    @BindView(R.id.recycler_view_popular_brands) RecyclerView recyclerViewBrands;
    @BindView(R.id.recycler_view_star_products) RecyclerView recyclerViewStarProducts;
    @BindView(R.id.txt_view_cart_count)
    TextView txtViewCartCount;
    @BindView(R.id.txt_view_wallet)
    TextView txtViewWallet;
    @BindView(R.id.app_rating_bar)
    RatingBar ratingBar;
    @BindView(R.id.edit_text_review)
    EditText reviewEditText;
    @BindView(R.id.recycler_view_reviews)
    RecyclerView recyclerView;
    MainPresenterImpl mPresenter;
    private static int currentPage = 0;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        inflateContent(R.layout.activity_main);
        txtViewWallet.setVisibility(View.GONE);
        setRecyclerViewBrands();
        initialisePresenter();
        setUpProgressDialog();
        mPresenter.getCategories();
        mPresenter.getMainData();
        mPresenter.fetchReviews();
    }

    private void initialisePresenter() {
        mPresenter = new MainPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    private void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    public void prepareDotsIndicator(int sliderPosition, int imageCount){
        if(dotsLinearLayout.getChildCount() > 0){
            dotsLinearLayout.removeAllViews();
        }

        ImageView dots[] = new ImageView[imageCount];
        for(int i = 0; i < imageCount; i++){
            dots[i] = new ImageView(this);
            if(i == sliderPosition){
                GlideHelper.setImageViewWithDrawable(getApplicationContext(), dots[i], R.drawable.active_dot);
            }else{
                GlideHelper.setImageViewWithDrawable(getApplicationContext(), dots[i], R.drawable.inactive_dot);
            }

            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(18, 18);
            layoutParams.setMargins(5, 0, 5, 0);
            dotsLinearLayout.addView(dots[i], layoutParams);

        }
    }

    public void setRecyclerViewBrands() {
        Brands brands1 = new Brands("https://media6.ppl-media.com/tr:h-750,w-750,c-at_max/static/img/product/152243/sunsilk-hairfall-solution-shampoo-180-ml-58_5_display_1565177201_afc58643.jpg", "Sunsilk");
        Brands brands2 = new Brands("https://yt3.ggpht.com/a/AGF-l78ggIkZYVDu3ZMHhkY7tMRCzMs6zzP6XqiEcA=s900-c-k-c0xffffffff-no-rj-mo", "Parle");
        Brands brands3 = new Brands("https://www.brandlanes.com/blog/wp-content/uploads/2019/11/105-1.jpg", "Lux");
        Brands brands4 = new Brands("https://i.pinimg.com/originals/16/19/68/1619680f465f3bc2806243919e9f9c0d.jpg", "Dettol");
        Brands brands6 = new Brands("https://mckeenmetroglebe.com/wp-content/uploads/2019/06/Pantene-Pro-V-Logo.png", "Pantene");
        Brands brands7 = new Brands("https://pbs.twimg.com/profile_images/1116764764552814592/jRzj037T_400x400.png", "Tresemme");
        Brands brands9 = new Brands("https://brandyuva.in/wp-content/uploads/2018/09/lipton-brand-success.jpg", "Lipton");
        Brands brands10 = new Brands("https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQa7m7iT08tH_GBQs7vnVhqHIQ1lZrL-boGeI_0ixyb_ChSU9VZ&usqp=CAU", "Bisk Farm");
        Brands brands11 = new Brands("https://www.mbaskool.com/2015_images/stories/Popular_2015/meaning-logos/meaning-logos02.jpg", "Bisk Farm");
        Brands brands12 = new Brands("https://prod-cdn.thekrazycouponlady.com/wp-content/uploads/2015/09/stayfree-logo-1533444262.png", "Stay Free");
        Brands brands13 = new Brands("https://www.govmint.com/media/catalog/product/cache/94798ddef09e704c73fc83615182db87/3/3/336346_3.jpg", "Coca Cola");
        Brands brands14 = new Brands("https://3.bp.blogspot.com/-OIRfnflXrm0/WDqusJLNocI/AAAAAAAAFY4/Lvc5ok9_Ju8zsgUIa3O8Fi7NJpkw6TEKgCLcB/s1600/britannia.jpg", "Britannia");
        Brands brands15 = new Brands("https://images.all-free-download.com/images/graphicthumb/nan_108447.jpg", "NAN");
        Brands brands16 = new Brands("https://mk0ehealtheletsj3t14.kinstacdn.com/wp-content/uploads/2016/05/Patanjali-logo.jpg", "Patanjali");
        Brands brands17 = new Brands("https://www.brandlanes.com/blog/wp-content/uploads/2019/11/111-1-1-1.jpg", "Amul");
        Brands brands18 = new Brands("https://www.huggies.com/Content/img/Android_App_Icon.png", "Huggies");
        Brands brands19 = new Brands("https://suvadin.com/uploads/info/1484546783_14316787_1786755681603277_4966839286517277349_n.png", "Real");
        Brands brands20 = new Brands("https://www.foodshow.co.nz/files/partner_files/9315/5426/3278/1553_1554263272.jpg", "Maggi");
        Brands brands21 = new Brands("https://i.pinimg.com/originals/1f/d6/46/1fd646ad1cf3569fd44ec147a31b5b53.jpg", "Cadburry");
        Brands brands22 = new Brands("https://cached.imagescaler.hbpl.co.uk/resize/scaleWidth/815/cached.offlinehbpl.hbpl.co.uk/news/ORP/Kelloggs_logo-20161208032851301.jpg", "Kellogs");
        Brands brands23 = new Brands("https://i4.aroq.com/3/2018-03-28-09-23-horlickslogo_cropped_90.jpg", "Horlicks");
        Brands brands24 = new Brands("https://i.ytimg.com/vi/Q_kWIxYA71A/maxresdefault.jpg", "Lays");
        Brands brands25 = new Brands("https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTeozDCrNxSMBT60OEWEbEAjYCD09YFh9IsaCaXzaJWB_NEPgGm&usqp=CAU", "Fortune");
        Brands brands26 = new Brands("https://mma.prnewswire.com/media/1061180/COLGATE_Logo.jpg", "Colgate");
        Brands brands27 = new Brands("https://www.jkspices.com/images/blended_spices.png", "JK Masala");
        Brands brands28 = new Brands("https://www.justgotochef.com/img/1520676900-MDH-Masala-Chana-Logo.jpg", "MDH");
        Brands brands29 = new Brands("https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTMnCLq_qSEkG0iGx2LP9G7b0LoCF93pQr9e4wTACEAcciCk-SV&usqp=CAU", "Dove");
        Brands brands30 = new Brands("https://www.smartpixels.fr/wp-content/uploads/2015/11/LOreal-Web1.jpg", "Loreal");
        Brands brands31 = new Brands("https://www.unileverusa.com/Images/Ponds-logo-2016_tcm1269-408782.jpg", "Ponds");
        Brands brands32 = new Brands("https://www.advertwiki.com/wp-content/uploads/2019/09/unnamed.jpg", "Aashirvaad");
        Brands brands33 = new Brands("https://pbs.twimg.com/profile_images/515442889225011200/wNNUxEAm_400x400.jpeg", "Aashirvaad");
        Brands brands34 = new Brands("https://www.lookp.com/assets/logo/himalaya/himalaya-profile.jpg", "Himalaya");
        Brands brands35 = new Brands("https://3.bp.blogspot.com/-hscrOjihqZ8/WMPjC96d6UI/AAAAAAAAXZQ/0__1ZzO3ZVQJz--b0vcWPe7ma52bCP6PgCLcB/s1600/Colour%2BMatch%2BContest%2BAttractive%2Bprizes%2Bup%2Bfor%2Bgrabs.png", "Winkies");


        ArrayList<Brands> brandsArrayList = new ArrayList<>();
        brandsArrayList.add(brands1);
        brandsArrayList.add(brands2);
        brandsArrayList.add(brands3);
        brandsArrayList.add(brands4);
        brandsArrayList.add(brands6);
        brandsArrayList.add(brands7);
        brandsArrayList.add(brands9);
        brandsArrayList.add(brands10);
        brandsArrayList.add(brands11);
        brandsArrayList.add(brands12);
        brandsArrayList.add(brands13);
        brandsArrayList.add(brands14);
        brandsArrayList.add(brands15);
        brandsArrayList.add(brands16);
        brandsArrayList.add(brands17);
        brandsArrayList.add(brands18);
        brandsArrayList.add(brands19);
        brandsArrayList.add(brands20);
        brandsArrayList.add(brands21);
        brandsArrayList.add(brands22);
        brandsArrayList.add(brands23);
        brandsArrayList.add(brands24);
        brandsArrayList.add(brands25);
        brandsArrayList.add(brands26);
        brandsArrayList.add(brands27);
        brandsArrayList.add(brands28);
        brandsArrayList.add(brands29);
        brandsArrayList.add(brands30);
        brandsArrayList.add(brands31);
        brandsArrayList.add(brands32);
        brandsArrayList.add(brands33);
        brandsArrayList.add(brands34);
        brandsArrayList.add(brands35);

        BrandsAdapter brandsAdapter = new BrandsAdapter(this, brandsArrayList);
        recyclerViewBrands.setAdapter(brandsAdapter);
        recyclerViewBrands.setLayoutManager(new GridLayoutManager(this, 3));
        recyclerViewBrands.setNestedScrollingEnabled(false);
    }

    @Override
    public void loadCategoriesAdapter(MainCategoriesAdapter adapter) {
        recyclerViewCategories.setAdapter(adapter);
        recyclerViewCategories.setLayoutManager(new GridLayoutManager(this, 2));
    }

    @Override
    public void goToSubcategoryActivity(int catId, String categoryName) {
        Intent intent = new Intent(this, SubcategoryActivity.class);
        intent.putExtra("catId", catId);
        intent.putExtra("categoryName", categoryName);
        startActivity(intent);
    }

    @Override
    public void goToProductList(int categoryId, String categoryName) {
        Intent intent = new Intent(this, ProductListActivity.class);
        intent.putExtra("categoryId", categoryId);
        intent.putExtra("type", 1);
        intent.putExtra("categoryName", categoryName);
        startActivity(intent);
    }

    @Override
    public void loadMainData(ProductHorizontalAdapter newProductsAdapter, ProductHorizontalAdapter starProductsAdapter, ImageSlider[] imageSliders) {

        recyclerViewStarProducts.setAdapter(starProductsAdapter);
        recyclerViewStarProducts.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        HomeSlidingImagesAdapter homeSlidingImagesAdapter = new HomeSlidingImagesAdapter(this, imageSliders);
        viewPager.setAdapter(homeSlidingImagesAdapter);
        prepareDotsIndicator(0, imageSliders.length);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                prepareDotsIndicator(position, imageSliders.length);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        // Auto start of viewpager
        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if(currentPage == imageSliders.length)
                {
                    currentPage = 0;
                }
                viewPager.setCurrentItem(currentPage, true);
                currentPage++;
            }
        };

        Timer swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(Update);
            }
        }, 1000, 3000);

    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    @Override
    public void goToProductDetails(int productId) {
        Intent intent = new Intent(this, ProductDetailsActivity.class);
        intent.putExtra("productId", productId);
        startActivity(intent);
    }

    @Override
    public void loadCartCount(int count) {
        if (count == 0) {
            txtViewCartCount.setVisibility(View.GONE);
        } else {
            txtViewCartCount.setVisibility(View.VISIBLE);
            txtViewCartCount.setText(Integer.toString(count));
        }
    }

    @Override
    public void loadStarProductsCount(int count) {

    }

    @Override
    public void loadWalletAmount(double walletAmount) {
        txtViewWallet.setVisibility(View.VISIBLE);
        txtViewWallet.setText("Free Rs. " + walletAmount);
    }

    @Override
    public void loadReviews(ReviewsAdapter adapter) {
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPresenter.fetchCartCount();
        mPresenter.fetchStarProductsCount();
        mPresenter.fetchWalletAmount();
    }

    @OnClick(R.id.btn_submit_review) void onSubmitBtnClicked(){
        androidApplication = (AndroidApplication) getApplicationContext();
        if (androidApplication.getUserInfo(this) == null) {
            Toast.makeText(this, "Please login to give feedback", Toast.LENGTH_SHORT).show();
        } else {
            if(reviewEditText.getText().toString().isEmpty()) {
                Toast.makeText(this, "Please give your feedback", Toast.LENGTH_SHORT).show();
            } else if (ratingBar.getRating() < 1){
                Toast.makeText(this, "Please give rating", Toast.LENGTH_SHORT).show();
            } else {
                mPresenter.submitReview(Math.round(ratingBar.getRating()), reviewEditText.getText().toString());
            }
        }
    }

}
