package com.webinfotech.santirekha2.presentation.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.webinfotech.santirekha2.R;
import com.webinfotech.santirekha2.domain.models.Review.Review;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Raj on 28-02-2019.
 */

public class ReviewsAdapter extends RecyclerView.Adapter<ReviewsAdapter.ViewHolder> {

    Context mContext;
    Review[] reviews;

    public ReviewsAdapter(Context mContext, Review[] reviews) {
        this.mContext = mContext;
        this.reviews = reviews;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_reviews, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtViewUsername.setText(reviews[position].customer_name);
        holder.txtViewComment.setText(reviews[position].comment);
        holder.txtViewDate.setText(reviews[position].created_at);
        holder.ratingBar.setRating(reviews[position].star);
    }

    @Override
    public int getItemCount() {
        return reviews.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        View mView;
        @BindView(R.id.recycler_view_review_comment)
        TextView txtViewComment;
        @BindView(R.id.recycler_view_review_date)
        TextView txtViewDate;
        @BindView(R.id.recycler_view_review_username)
        TextView txtViewUsername;
        @BindView(R.id.recycler_view_review_ratingbar)
        RatingBar ratingBar;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            this.mView = itemView;
        }
    }

}
