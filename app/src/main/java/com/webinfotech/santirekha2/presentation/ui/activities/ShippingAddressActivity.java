package com.webinfotech.santirekha2.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import com.webinfotech.santirekha2.R;
import com.webinfotech.santirekha2.domain.executors.impl.ThreadExecutor;
import com.webinfotech.santirekha2.presentation.presenters.ShippingAddressPresenter;
import com.webinfotech.santirekha2.presentation.presenters.impl.ShippingAddressPresenterImpl;
import com.webinfotech.santirekha2.presentation.ui.adapters.ShippingAddressAdapter;
import com.webinfotech.santirekha2.threading.MainThreadImpl;

public class ShippingAddressActivity extends AppCompatActivity implements ShippingAddressPresenter.View {

    @BindView(R.id.recycler_view_shipping_address)
    RecyclerView recyclerViewAddress;
    ShippingAddressPresenterImpl mPresenter;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shipping_address);
        getSupportActionBar().setTitle("Shipping Address");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ButterKnife.bind(this);
        initialisePresenter();
        setUpProgressDialog();
    }

    private void initialisePresenter() {
        mPresenter = new ShippingAddressPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    private void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    @Override
    public void setAddressAdapter(ShippingAddressAdapter addressAdapter) {
        recyclerViewAddress.setLayoutManager(new LinearLayoutManager(this));
        recyclerViewAddress.setAdapter(addressAdapter);
    }

    @Override
    public void goToEditAddressActivity(int position) {
        Intent intent = new Intent(this, EditAddressActivity.class);
        intent.putExtra("position", position);
        startActivity(intent);
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPresenter.fetchShippingAddress();
    }

    @OnClick(R.id.layout_add_address) void onAddAddressClicked() {
        Intent intent = new Intent(this, AddAddressActivity.class);
        startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


}
