package com.webinfotech.santirekha2.domain.interactors;

public interface DeleteShippingAddressInteractor {
    interface Callback {
        void onAddressDeleteSuccess();
        void onAddressDeleteFail(String errorMsg);
    }
}
