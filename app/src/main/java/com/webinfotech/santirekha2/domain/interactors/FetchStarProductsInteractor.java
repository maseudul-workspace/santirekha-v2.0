package com.webinfotech.santirekha2.domain.interactors;

import com.webinfotech.santirekha2.domain.models.Products.StarProduct;

public interface FetchStarProductsInteractor {
    interface Callback {
        void onGettingStarProductsSuccess(StarProduct[] starProducts, int totalPage);
        void onGettingStarProductsFail(String errorMsg);
    }
}
