package com.webinfotech.santirekha2.domain.interactors;

public interface CreateUserInteractor {
    interface Callback {
        void onUserCreateSuccess();
        void onUserCreateFail(String errorMsg);
    }
}
