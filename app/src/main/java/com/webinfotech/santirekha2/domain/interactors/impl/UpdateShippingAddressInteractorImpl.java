package com.webinfotech.santirekha2.domain.interactors.impl;

import com.webinfotech.santirekha2.domain.executors.Executor;
import com.webinfotech.santirekha2.domain.executors.MainThread;
import com.webinfotech.santirekha2.domain.interactors.UpdateShippingAddressInteractor;
import com.webinfotech.santirekha2.domain.interactors.base.AbstractInteractor;
import com.webinfotech.santirekha2.domain.models.User.UpdateShippingAddressResponse;
import com.webinfotech.santirekha2.repository.User.UserRepositoryImpl;

public class UpdateShippingAddressInteractorImpl extends AbstractInteractor implements UpdateShippingAddressInteractor {

    UserRepositoryImpl mRepository;
    Callback mCallback;
    int userId;
    String apiKey;
    int addressId;
    String mobile;
    String email;
    String state;
    String city;
    String address;
    String pin;

    public UpdateShippingAddressInteractorImpl(Executor threadExecutor, MainThread mainThread, UserRepositoryImpl mRepository, Callback mCallback, int userId, String apiKey, int addressId, String mobile, String email, String state, String city, String address, String pin) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.userId = userId;
        this.apiKey = apiKey;
        this.addressId = addressId;
        this.mobile = mobile;
        this.email = email;
        this.state = state;
        this.city = city;
        this.address = address;
        this.pin = pin;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onEditAddressFail(errorMsg);
            }
        });
    }

    private void postMessage(){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onEditAddressSuccess();
            }
        });
    }

    @Override
    public void run() {
        final UpdateShippingAddressResponse updateShippingAddressResponse = mRepository.updateShippingAddress(userId, apiKey, addressId, mobile, email, state, city, address, pin);
        if (updateShippingAddressResponse == null) {
            notifyError("Something Went Wrong");
        } else if (!updateShippingAddressResponse.status) {
            notifyError(updateShippingAddressResponse.message);
        } else {
           postMessage();
        }
    }

}
