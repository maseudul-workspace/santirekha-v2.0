package com.webinfotech.santirekha2.domain.models.Wallet;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Raj on 21-02-2019.
 */

public class WalletStatus {

    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("user_id")
    @Expose
    public int userId;

    @SerializedName("total_amount")
    @Expose
    public double totalAmount;

}
