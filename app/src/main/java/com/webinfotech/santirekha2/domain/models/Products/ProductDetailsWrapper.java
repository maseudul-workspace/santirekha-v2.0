package com.webinfotech.santirekha2.domain.models.Products;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.webinfotech.santirekha2.domain.models.Products.Product;

/**
 * Created by Raj on 19-02-2019.
 */

public class ProductDetailsWrapper {

    @SerializedName("status")
    @Expose
    public Boolean status;

    @SerializedName("message")
    @Expose
    public String message;

    @SerializedName("code")
    @Expose
    public int code;

    @SerializedName("data")
    @Expose
    public ProductDetailsData products;


}
