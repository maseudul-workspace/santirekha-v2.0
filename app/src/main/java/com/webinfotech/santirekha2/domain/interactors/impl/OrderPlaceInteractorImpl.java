package com.webinfotech.santirekha2.domain.interactors.impl;

import com.webinfotech.santirekha2.domain.executors.Executor;
import com.webinfotech.santirekha2.domain.executors.MainThread;
import com.webinfotech.santirekha2.domain.interactors.OrderPlaceInteractor;
import com.webinfotech.santirekha2.domain.interactors.base.AbstractInteractor;
import com.webinfotech.santirekha2.domain.models.Orders.OrderPlacedResponse;
import com.webinfotech.santirekha2.repository.User.UserRepositoryImpl;

/**
 * Created by Raj on 26-02-2019.
 */

public class OrderPlaceInteractorImpl extends AbstractInteractor implements OrderPlaceInteractor {

    UserRepositoryImpl mRepository;
    Callback mCallback;
    String apiKey;
    int userId;
    int walletStatus;
    int shippingAddressId;

    public OrderPlaceInteractorImpl(Executor threadExecutor,
                                    MainThread mainThread,
                                    Callback callback,
                                    UserRepositoryImpl repository,
                                    String apiKey,
                                    int userId,
                                    int walletStatus,
                                    int shippingAddressId
                                    ) {
        super(threadExecutor, mainThread);
        this.mRepository = repository;
        this.mCallback = callback;
        this.apiKey = apiKey;
        this.userId = userId;
        this.walletStatus = walletStatus;
        this.shippingAddressId = shippingAddressId;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onOrderPlacedFail(errorMsg);
            }
        });
    }

    private void postMessage(){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onOrderPlacedSuccess();
            }
        });
    }


    @Override
    public void run() {
        final OrderPlacedResponse orderPlacedResponse = mRepository.placeOrder(apiKey, userId, walletStatus, shippingAddressId);
        if(orderPlacedResponse == null){
            notifyError("Something went wrong");
        }else if(!orderPlacedResponse.status){
            notifyError(orderPlacedResponse.message);
        }else{
            postMessage();
        }
    }
}
