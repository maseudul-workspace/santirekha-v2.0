package com.webinfotech.santirekha2.domain.interactors;


import com.webinfotech.santirekha2.domain.models.Category.SubCategory;

public interface GetSubcategoryListInteractor {
    interface Callback {
        void onGettingSubcategoriesSuccess(SubCategory[] subcategories);
        void onGettingSubcategoriesFail(String errorMsg);
    }
}
