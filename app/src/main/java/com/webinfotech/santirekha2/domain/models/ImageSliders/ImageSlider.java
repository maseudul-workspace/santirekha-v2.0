package com.webinfotech.santirekha2.domain.models.ImageSliders;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Raj on 24-06-2019.
 */

public class ImageSlider {

    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("image")
    @Expose
    public String image;

    @SerializedName("title")
    @Expose
    public String title;

}
