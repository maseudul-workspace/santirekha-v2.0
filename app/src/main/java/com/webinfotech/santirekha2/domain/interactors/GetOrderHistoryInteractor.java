package com.webinfotech.santirekha2.domain.interactors;

import com.webinfotech.santirekha2.domain.models.Orders.OrderHistory;

/**
 * Created by Raj on 22-02-2019.
 */

public interface GetOrderHistoryInteractor {
    interface Callback{
        void onGettingOrderHistorySuccess(OrderHistory[] orderHistories);
        void onGettingOrderHistoryFail(String errorMsg);
    }
}
