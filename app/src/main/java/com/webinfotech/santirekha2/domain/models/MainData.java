package com.webinfotech.santirekha2.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.webinfotech.santirekha2.domain.models.ImageSliders.ImageSlider;
import com.webinfotech.santirekha2.domain.models.Products.Product;

public class MainData {

    @SerializedName("sliders")
    @Expose
    public ImageSlider[] imageSliders;

    @SerializedName("new_arrivals")
    @Expose
    public Product[] newArrivals;

    @SerializedName("star_products")
    @Expose
    public Product[] startProducts;

}
