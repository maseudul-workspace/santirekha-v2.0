package com.webinfotech.santirekha2.domain.interactors;

import com.webinfotech.santirekha2.domain.models.Products.Product;
import com.webinfotech.santirekha2.domain.models.Products.ProductDetailsData;

public interface FetchProductDetailsInteractor {
    interface Callback {
        void onGettingProductDetailsSuccess(ProductDetailsData product);
        void onGettingProductDetailsFail(String errorMsg);
    }
}
