package com.webinfotech.santirekha2.domain.interactors.impl;

import com.webinfotech.santirekha2.domain.executors.Executor;
import com.webinfotech.santirekha2.domain.executors.MainThread;
import com.webinfotech.santirekha2.domain.interactors.GetMainDataInteractor;
import com.webinfotech.santirekha2.domain.interactors.GetProductListInteractor;
import com.webinfotech.santirekha2.domain.interactors.base.AbstractInteractor;
import com.webinfotech.santirekha2.domain.models.MainData;
import com.webinfotech.santirekha2.domain.models.MainDataWrapper;
import com.webinfotech.santirekha2.repository.ProductRepository.impl.GetProductsRepositoryImpl;

public class GetMainDataInteractorImpl extends AbstractInteractor implements GetMainDataInteractor {

    GetProductsRepositoryImpl mRepository;
    Callback mCallback;

    public GetMainDataInteractorImpl(Executor threadExecutor, MainThread mainThread, GetProductsRepositoryImpl mRepository, Callback mCallback) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingMainDataFail(errorMsg);
            }
        });
    }

    private void postMessage(MainData mainData) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingMainDataSuccess(mainData);
            }
        });
    }

    @Override
    public void run() {
        final MainDataWrapper mainDataWrapper = mRepository.getMainData();
        if (mainDataWrapper == null) {
            notifyError("Something Went Wrong");
        } else if (!mainDataWrapper.status) {
            notifyError(mainDataWrapper.message);
        } else {
            postMessage(mainDataWrapper.mainData);
        }
    }
}
