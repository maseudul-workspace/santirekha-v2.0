package com.webinfotech.santirekha2.domain.models.Products;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProductDetailsData {

    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("name")
    @Expose
    public String name;

    @SerializedName("description")
    @Expose
    public String description;

    @SerializedName("category")
    @Expose
    public String category;

    @SerializedName("type")
    @Expose
    public String type;

    @SerializedName("stock")
    @Expose
    public int stock;

    @SerializedName("mrp")
    @Expose
    public double mrp;

    @SerializedName("price")
    @Expose
    public double price;

    @SerializedName("cash_back")
    @Expose
    public double cashBack;

    @SerializedName("promotional_bonus")
    @Expose
    public double promoBonus;

    @SerializedName("image")
    @Expose
    public String image;

    @SerializedName("related_products")
    @Expose
    public Product[] products;

}
