package com.webinfotech.santirekha2.domain.interactors.impl;

import com.webinfotech.santirekha2.domain.executors.Executor;
import com.webinfotech.santirekha2.domain.executors.MainThread;
import com.webinfotech.santirekha2.domain.interactors.GetCreditHistoryInteractor;
import com.webinfotech.santirekha2.domain.interactors.base.AbstractInteractor;
import com.webinfotech.santirekha2.domain.models.Wallet.WalletDetails;
import com.webinfotech.santirekha2.domain.models.Wallet.WalletDetailsWrapper;
import com.webinfotech.santirekha2.repository.User.UserRepositoryImpl;

public class GetCreditHistoryInteractorImpl extends AbstractInteractor implements GetCreditHistoryInteractor {

    UserRepositoryImpl mRepository;
    Callback mCallback;
    String apiKey;
    int userId;
    int page;

    public GetCreditHistoryInteractorImpl(Executor threadExecutor, MainThread mainThread, UserRepositoryImpl mRepository, Callback mCallback, String apiKey, int userId, int page) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.apiKey = apiKey;
        this.userId = userId;
        this.page = page;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingCreditHistoryFail(errorMsg);
            }
        });
    }

    private void postMessage(final WalletDetails walletDetails, int totalPage){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingCreditHistorySuccess(walletDetails, totalPage);
            }
        });
    }

    @Override
    public void run() {
        final WalletDetailsWrapper walletDetailsWrapper = mRepository.getCreditHistory(apiKey, userId, page);
        if(walletDetailsWrapper == null){
            notifyError("Something went wrong !!! Please check your internet");
        }else if(!walletDetailsWrapper.status){
            notifyError(walletDetailsWrapper.message);
        }else{
            postMessage(walletDetailsWrapper.walletDetails, walletDetailsWrapper.totalPage);
        }
    }
}
