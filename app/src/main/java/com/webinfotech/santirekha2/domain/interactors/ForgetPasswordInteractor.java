package com.webinfotech.santirekha2.domain.interactors;

/**
 * Created by Raj on 25-06-2019.
 */

public interface ForgetPasswordInteractor {
    interface Callback{
        void onForgetPasswordSuccess(String successMsg);
        void onForgetPasswordFail(String errorMsg);
    }
}
