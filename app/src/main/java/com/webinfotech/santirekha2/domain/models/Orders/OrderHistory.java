package com.webinfotech.santirekha2.domain.models.Orders;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Raj on 22-02-2019.
 */

public class OrderHistory {

    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("date")
    @Expose
    public String date;

    @SerializedName("total")
    @Expose
    public Long total;

    @SerializedName("cashback")
    @Expose
    public Double cashback;

    @SerializedName("payable_amount")
    @Expose
    public Double payable_amount;

    @SerializedName("wallet_pay")
    @Expose
    public Double wallet_pay;

    @SerializedName("delivery_status")
    @Expose
    public int delivery_status;

    @SerializedName("products")
    @Expose
    public OrderProducts[] products;

}
