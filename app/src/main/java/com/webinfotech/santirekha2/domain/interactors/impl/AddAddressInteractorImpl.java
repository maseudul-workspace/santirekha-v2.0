package com.webinfotech.santirekha2.domain.interactors.impl;

import com.webinfotech.santirekha2.domain.executors.Executor;
import com.webinfotech.santirekha2.domain.executors.MainThread;
import com.webinfotech.santirekha2.domain.interactors.AddAddressInteractor;
import com.webinfotech.santirekha2.domain.interactors.base.AbstractInteractor;
import com.webinfotech.santirekha2.domain.models.CommonResponse;
import com.webinfotech.santirekha2.domain.models.User.ShippingAddressAddResponse;
import com.webinfotech.santirekha2.repository.User.UserRepositoryImpl;

public class AddAddressInteractorImpl extends AbstractInteractor implements AddAddressInteractor {

    UserRepositoryImpl mRepository;
    Callback mCallback;
    int userId;
    String apiKey;
    String mobile;
    String email;
    String state;
    String city;
    String address;
    String pin;

    public AddAddressInteractorImpl(Executor threadExecutor, MainThread mainThread, UserRepositoryImpl mRepository, Callback mCallback, int userId, String apiKey, String mobile, String email, String state, String city, String address, String pin) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.userId = userId;
        this.apiKey = apiKey;
        this.mobile = mobile;
        this.email = email;
        this.state = state;
        this.city = city;
        this.address = address;
        this.pin = pin;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onAddAddressFail(errorMsg);
            }
        });
    }

    private void postMessage(){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onAddressAddSuccess();
            }
        });
    }

    @Override
    public void run() {
        final ShippingAddressAddResponse shippingAddressAddResponse = mRepository.addShippingAddress(userId, apiKey, mobile, email, state, city, address, pin);
        if (shippingAddressAddResponse == null) {
            notifyError(shippingAddressAddResponse.message);
        } else if (!shippingAddressAddResponse.status) {
            notifyError(shippingAddressAddResponse.message);
        } else {
            postMessage();
        }
    }
}
