package com.webinfotech.santirekha2.domain.interactors.impl;

import com.webinfotech.santirekha2.domain.executors.Executor;
import com.webinfotech.santirekha2.domain.executors.MainThread;
import com.webinfotech.santirekha2.domain.interactors.GetCategoryListInteractor;
import com.webinfotech.santirekha2.domain.interactors.base.AbstractInteractor;
import com.webinfotech.santirekha2.domain.models.Category.Category;
import com.webinfotech.santirekha2.domain.models.Category.CategoryListWrapper;
import com.webinfotech.santirekha2.repository.Category.CategoryRepositoryImpl;

public class GetCategoryListInteractorImpl extends AbstractInteractor implements GetCategoryListInteractor {

    CategoryRepositoryImpl mRepository;
    Callback mCallback;

    public GetCategoryListInteractorImpl(Executor threadExecutor, MainThread mainThread, CategoryRepositoryImpl mRepository, Callback mCallback) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingCategoryFail(errorMsg);
            }
        });
    }

    private void postMessage(Category[] categories){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingCategoryListSuccess(categories);
            }
        });
    }

    @Override
    public void run() {
        final CategoryListWrapper categoryListWrapper = mRepository.getCategoryList();
        if (categoryListWrapper == null) {
            notifyError("Something Went Wrong");
        } else if (!categoryListWrapper.status) {
            notifyError(categoryListWrapper.message);
        } else {
            postMessage(categoryListWrapper.categories);
        }
    }
}
