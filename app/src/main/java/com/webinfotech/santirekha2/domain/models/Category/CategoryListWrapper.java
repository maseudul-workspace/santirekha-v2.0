package com.webinfotech.santirekha2.domain.models.Category;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.webinfotech.santirekha2.domain.models.Category.Category;

/**
 * Created by Raj on 11-02-2019.
 */

public class CategoryListWrapper {

    @SerializedName("status")
    @Expose
    public Boolean status;

    @SerializedName("message")
    @Expose
    public String message;

    @SerializedName("code")
    @Expose
    public int code;

    @SerializedName("data")
    @Expose
    public Category[] categories;

}
