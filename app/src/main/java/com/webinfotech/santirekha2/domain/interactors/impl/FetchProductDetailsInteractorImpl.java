package com.webinfotech.santirekha2.domain.interactors.impl;

import com.webinfotech.santirekha2.domain.executors.Executor;
import com.webinfotech.santirekha2.domain.executors.MainThread;
import com.webinfotech.santirekha2.domain.interactors.FetchProductDetailsInteractor;
import com.webinfotech.santirekha2.domain.interactors.base.AbstractInteractor;
import com.webinfotech.santirekha2.domain.models.Products.Product;
import com.webinfotech.santirekha2.domain.models.Products.ProductDetailsData;
import com.webinfotech.santirekha2.domain.models.Products.ProductDetailsWrapper;
import com.webinfotech.santirekha2.repository.ProductRepository.impl.GetProductsRepositoryImpl;

public class FetchProductDetailsInteractorImpl extends AbstractInteractor implements FetchProductDetailsInteractor {

    GetProductsRepositoryImpl mRepository;
    Callback mCallback;
    int productId;

    public FetchProductDetailsInteractorImpl(Executor threadExecutor, MainThread mainThread, GetProductsRepositoryImpl mRepository, Callback mCallback, int productId) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.productId = productId;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingProductDetailsFail(errorMsg);
            }
        });
    }

    private void postMessage(ProductDetailsData product){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingProductDetailsSuccess(product);
            }
        });
    }

    @Override
    public void run() {
        final ProductDetailsWrapper productDetailsWrapper = mRepository.getProductDetailsById(productId);
        if (productDetailsWrapper == null) {
            notifyError("Something Went Wrong");
        } else if (!productDetailsWrapper.status) {
            notifyError(productDetailsWrapper.message);
        } else {
            postMessage(productDetailsWrapper.products);
        }
    }
}
