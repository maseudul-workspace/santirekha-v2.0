package com.webinfotech.santirekha2.domain.interactors;

import com.webinfotech.santirekha2.domain.models.User.UserInfo;

public interface CheckLoginInteractor {
    interface Callback {
        void onLoginSuccess(UserInfo userInfo);
        void onLoginFail(String errorMsg);
    }
}
