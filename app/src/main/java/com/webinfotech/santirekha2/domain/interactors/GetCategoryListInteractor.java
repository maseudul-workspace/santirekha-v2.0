package com.webinfotech.santirekha2.domain.interactors;

import com.webinfotech.santirekha2.domain.models.Category.Category;

public interface GetCategoryListInteractor {
    interface Callback {
        void onGettingCategoryListSuccess(Category[] categories);
        void onGettingCategoryFail(String errorMsg);
    }
}
