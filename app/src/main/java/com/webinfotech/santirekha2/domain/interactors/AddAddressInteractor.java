package com.webinfotech.santirekha2.domain.interactors;

public interface AddAddressInteractor {
    interface Callback {
        void onAddressAddSuccess();
        void onAddAddressFail(String errorMsg);
    }
}
