package com.webinfotech.santirekha2.domain.interactors.impl;

import com.webinfotech.santirekha2.domain.executors.Executor;
import com.webinfotech.santirekha2.domain.executors.MainThread;
import com.webinfotech.santirekha2.domain.interactors.FetchStarProductsInteractor;
import com.webinfotech.santirekha2.domain.interactors.base.AbstractInteractor;
import com.webinfotech.santirekha2.domain.models.Products.StarProduct;
import com.webinfotech.santirekha2.domain.models.Products.StarProductsWrapper;
import com.webinfotech.santirekha2.repository.ProductRepository.impl.GetProductsRepositoryImpl;

public class FetchStarProductsInteractorImpl extends AbstractInteractor implements FetchStarProductsInteractor {

    GetProductsRepositoryImpl mRepository;
    Callback mCallback;
    int userId;
    String apiToken;
    int pageNo;

    public FetchStarProductsInteractorImpl(Executor threadExecutor, MainThread mainThread, GetProductsRepositoryImpl mRepository, Callback mCallback, int userId, String apiToken, int pageNo) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.userId = userId;
        this.apiToken = apiToken;
        this.pageNo = pageNo;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingStarProductsFail(errorMsg);
            }
        });
    }

    private void postMessage(final StarProduct[] starProducts,  int totalPage){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingStarProductsSuccess(starProducts, totalPage);
            }
        });
    }

    @Override
    public void run() {
        StarProductsWrapper starProductsWrapper = mRepository.fetchStarProducts(userId, apiToken, pageNo);
        if (starProductsWrapper == null) {
            notifyError("Something Went Wrong");
        } else if (!starProductsWrapper.status) {
            notifyError(starProductsWrapper.message);
        } else {
            postMessage(starProductsWrapper.starProducts, starProductsWrapper.totalPage);
        }
    }
}
