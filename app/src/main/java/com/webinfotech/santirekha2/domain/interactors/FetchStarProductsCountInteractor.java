package com.webinfotech.santirekha2.domain.interactors;

public interface FetchStarProductsCountInteractor {
    interface Callback {
        void onGettingStarProductsCountSuccess(int starProductsCount);
        void onGettingStarProductsCountFail(String errorMsg);
    }
}
