package com.webinfotech.santirekha2.domain.interactors.impl;

import com.webinfotech.santirekha2.domain.executors.Executor;
import com.webinfotech.santirekha2.domain.executors.MainThread;
import com.webinfotech.santirekha2.domain.interactors.SubmitReviewInteractor;
import com.webinfotech.santirekha2.domain.interactors.base.AbstractInteractor;
import com.webinfotech.santirekha2.domain.models.Review.ReviewSubmitResponse;
import com.webinfotech.santirekha2.repository.User.UserRepositoryImpl;
/**
 * Created by Raj on 28-02-2019.
 */

public class SubmitReviewInteractorImpl extends AbstractInteractor implements SubmitReviewInteractor {

    UserRepositoryImpl mRepository;
    Callback mCallback;
    String apiKey;
    int userId;
    int rating;
    String comments;

    public SubmitReviewInteractorImpl(Executor threadExecutor,
                                      MainThread mainThread,
                                      Callback callback,
                                      UserRepositoryImpl repository,
                                      int userId,
                                      String apiKey,
                                      int rating,
                                      String comments
                                      ) {
        super(threadExecutor, mainThread);
        this.mCallback = callback;
        this.apiKey = apiKey;
        this.rating = rating;
        this.comments = comments;
        this.userId = userId;
        mRepository = repository;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onSubmitReviewFail(errorMsg);
            }
        });
    }

    private void postMessage(final String successMsg){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onSubmitReviewSuccess(successMsg);
            }
        });
    }


    @Override
    public void run() {
        final ReviewSubmitResponse reviewSubmitResponse = mRepository.submitReview(apiKey, userId, rating, comments);
        if(reviewSubmitResponse == null){
            notifyError("Something went wrong");
        }else if(!reviewSubmitResponse.status){
            notifyError(reviewSubmitResponse.message);
        }else{
            postMessage(reviewSubmitResponse.message);
        }
    }
}
