package com.webinfotech.santirekha2.domain.interactors.impl;

import com.webinfotech.santirekha2.domain.executors.Executor;
import com.webinfotech.santirekha2.domain.executors.MainThread;
import com.webinfotech.santirekha2.domain.interactors.CreateUserInteractor;
import com.webinfotech.santirekha2.domain.interactors.base.AbstractInteractor;
import com.webinfotech.santirekha2.domain.models.CommonResponse;
import com.webinfotech.santirekha2.repository.User.UserRepositoryImpl;

public class CreateUserInteractorImpl extends AbstractInteractor implements CreateUserInteractor {

    UserRepositoryImpl mRepository;
    Callback mCallback;
    String name;
    String mobile;
    String email;
    String password;
    String state;
    String city;
    String address;
    String referalCode;
    String pin;

    public CreateUserInteractorImpl(Executor threadExecutor, MainThread mainThread, UserRepositoryImpl mRepository, Callback mCallback, String name, String mobile, String email, String password, String state, String city, String address, String referalCode, String pin) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.name = name;
        this.mobile = mobile;
        this.email = email;
        this.password = password;
        this.state = state;
        this.city = city;
        this.address = address;
        this.referalCode = referalCode;
        this.pin = pin;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onUserCreateFail(errorMsg);
            }
        });
    }

    private void postMessage(){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onUserCreateSuccess();
            }
        });
    }

    @Override
    public void run() {
        final CommonResponse commonResponse = mRepository.signUp(name, mobile, email, password, state, city, address, referalCode, pin);
        if (commonResponse == null) {
            notifyError("Something Went Wrong");
        } else if (!commonResponse.status) {
            notifyError(commonResponse.message);
        } else {
            postMessage();
        }
    }
}
