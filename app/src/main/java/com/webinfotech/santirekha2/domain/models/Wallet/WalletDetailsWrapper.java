package com.webinfotech.santirekha2.domain.models.Wallet;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.webinfotech.santirekha2.domain.models.Wallet.WalletDetails;

/**
 * Created by Raj on 22-02-2019.
 */

public class WalletDetailsWrapper {

    @SerializedName("status")
    @Expose
    public Boolean status;

    @SerializedName("message")
    @Expose
    public String message;

    @SerializedName("total_page")
    @Expose
    public int totalPage;

    @SerializedName("data")
    @Expose
    public WalletDetails walletDetails;

}
