package com.webinfotech.santirekha2.domain.interactors;

/**
 * Created by Raj on 14-02-2019.
 */

public interface ChangePasswordInteractor {
    interface Callback{
        void onChangingPasswordSuccess(String successMsg);
        void onChangingPasswordFail(String errorMsg);
    }
}
