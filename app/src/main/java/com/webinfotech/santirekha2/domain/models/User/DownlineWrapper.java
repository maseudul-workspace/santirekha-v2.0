package com.webinfotech.santirekha2.domain.models.User;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.webinfotech.santirekha2.domain.models.User.Downline;

/**
 * Created by Raj on 27-02-2019.
 */

public class DownlineWrapper {
    @SerializedName("status")
    @Expose
    public Boolean status;

    @SerializedName("message")
    @Expose
    public String message;

    @SerializedName("code")
    @Expose
    public int code;

    @SerializedName("data")
    @Expose
    public Downline[] downlines;
}
