package com.webinfotech.santirekha2.domain.interactors.impl;

import com.webinfotech.santirekha2.domain.executors.Executor;
import com.webinfotech.santirekha2.domain.executors.MainThread;
import com.webinfotech.santirekha2.domain.interactors.FetchStarProductsCountInteractor;
import com.webinfotech.santirekha2.domain.interactors.base.AbstractInteractor;
import com.webinfotech.santirekha2.domain.models.StarProductsCountResponse;
import com.webinfotech.santirekha2.repository.User.UserRepositoryImpl;

public class FetchStarProductsCountInteractorImpl extends AbstractInteractor implements FetchStarProductsCountInteractor {

    UserRepositoryImpl mRepository;
    Callback mCallback;
    int userId;
    String apiKey;

    public FetchStarProductsCountInteractorImpl(Executor threadExecutor, MainThread mainThread, UserRepositoryImpl mRepository, Callback mCallback, int userId, String apiKey) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.userId = userId;
        this.apiKey = apiKey;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingStarProductsCountFail(errorMsg);
            }
        });
    }

    private void postMessage(int count){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingStarProductsCountSuccess(count);
            }
        });
    }

    @Override
    public void run() {
        final StarProductsCountResponse starProductsCountResponse = mRepository.fetchStartProductCount(apiKey, userId);
        if (starProductsCountResponse == null) {
            notifyError("Something Went Wrong");
        } else if (!starProductsCountResponse.status) {
            notifyError(starProductsCountResponse.message);
        } else {
            postMessage(starProductsCountResponse.number);
        }
    }
}
