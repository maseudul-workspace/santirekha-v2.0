package com.webinfotech.santirekha2.domain.interactors;

/**
 * Created by Raj on 26-02-2019.
 */

public interface OrderPlaceInteractor {
    interface Callback{
        void onOrderPlacedSuccess();
        void onOrderPlacedFail(String errorMsg);
    }
}
