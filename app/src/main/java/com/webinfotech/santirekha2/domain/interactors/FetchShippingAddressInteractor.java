package com.webinfotech.santirekha2.domain.interactors;

import com.webinfotech.santirekha2.domain.models.User.ShippingAddress;

public interface FetchShippingAddressInteractor {
    interface Callback {
        void onAddressFetchSuccess(ShippingAddress[] shippingAddresses);
        void onAddressFetchFail(String errorMsg);
    }
}
