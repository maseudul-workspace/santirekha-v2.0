package com.webinfotech.santirekha2.domain.models.User;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.webinfotech.santirekha2.domain.models.User.ShippingAddress;

/**
 * Created by Raj on 15-02-2019.
 */

public class ShippingAddressWrapper {
    @SerializedName("status")
    @Expose
    public Boolean status;

    @SerializedName("message")
    @Expose
    public String message;

    @SerializedName("code")
    @Expose
    public int code;

    @SerializedName("data")
    @Expose
    public ShippingAddress[] shippingAddresses;
}
