package com.webinfotech.santirekha2.domain.interactors;

import com.webinfotech.santirekha2.domain.models.Products.Product;

/**
 * Created by Raj on 27-02-2019.
 */

public interface SearchProductsInteractor {
    interface Callback{
        void onGettingProductsSuccess(Product[] products, String searchKey, int totalPage);
        void onGettingProductsFail(String searchKey);
    }
}
