package com.webinfotech.santirekha2.domain.interactors;

import com.webinfotech.santirekha2.domain.models.MainData;

public interface GetMainDataInteractor {
    interface Callback {
        void onGettingMainDataSuccess(MainData mainData);
        void onGettingMainDataFail(String errorMsg);
    }
}
