package com.webinfotech.santirekha2.domain.interactors;


import com.webinfotech.santirekha2.domain.interactors.base.Interactor;
import com.webinfotech.santirekha2.domain.models.Cart.Cart;

/**
 * Created by Raj on 10-01-2019.
 */

public interface GetCartDetailsInteractor extends Interactor {
    interface Callback{
        void onGetCartDetailsSuccess(Cart[] carts);
        void onGetCartDetailsFail(String errorMsg);
    }
}
