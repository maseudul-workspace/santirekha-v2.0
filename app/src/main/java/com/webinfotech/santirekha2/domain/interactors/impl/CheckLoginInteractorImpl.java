package com.webinfotech.santirekha2.domain.interactors.impl;

import com.webinfotech.santirekha2.domain.executors.Executor;
import com.webinfotech.santirekha2.domain.executors.MainThread;
import com.webinfotech.santirekha2.domain.interactors.CheckLoginInteractor;
import com.webinfotech.santirekha2.domain.interactors.base.AbstractInteractor;
import com.webinfotech.santirekha2.domain.models.User.UserInfo;
import com.webinfotech.santirekha2.domain.models.User.UserInfoWrapper;
import com.webinfotech.santirekha2.repository.User.UserRepositoryImpl;

public class CheckLoginInteractorImpl extends AbstractInteractor implements CheckLoginInteractor {

    UserRepositoryImpl mRepository;
    Callback mCallback;
    String email;
    String password;

    public CheckLoginInteractorImpl(Executor threadExecutor, MainThread mainThread, UserRepositoryImpl mRepository, Callback mCallback, String email, String password) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.email = email;
        this.password = password;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onLoginFail(errorMsg);
            }
        });
    }

    private void postMessage(UserInfo userInfo){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onLoginSuccess(userInfo);
            }
        });
    }

    @Override
    public void run() {
        final UserInfoWrapper userInfoWrapper = mRepository.logInUser(email, password);
        if (userInfoWrapper == null) {
            notifyError("Something Went Wrong");
        } else if (!userInfoWrapper.status) {
            notifyError(userInfoWrapper.message);
        } else {
            postMessage(userInfoWrapper.userInfo);
        }
    }
}
