package com.webinfotech.santirekha2.domain.models.Cart;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.webinfotech.santirekha2.domain.models.Cart.Cart;

/**
 * Created by Raj on 19-02-2019.
 */

public class CartListWrapper {
    @SerializedName("status")
    @Expose
    public Boolean status;

    @SerializedName("message")
    @Expose
    public String message;

    @SerializedName("data")
    @Expose
    public Cart[] carts;


}
