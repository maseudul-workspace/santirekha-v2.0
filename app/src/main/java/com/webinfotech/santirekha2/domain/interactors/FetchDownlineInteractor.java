package com.webinfotech.santirekha2.domain.interactors;

import com.webinfotech.santirekha2.domain.models.User.Downline;

/**
 * Created by Raj on 27-02-2019.
 */

public interface FetchDownlineInteractor {
    interface Callback{
        void onGettingDownlineListSuccess(Downline[] downlines);
        void onGettingDownlineListFail(String errorMsg);
    }
}
