package com.webinfotech.santirekha2.domain.interactors.impl;

import android.telecom.Call;
import android.util.Log;
import android.widget.RelativeLayout;

import com.webinfotech.santirekha2.domain.executors.Executor;
import com.webinfotech.santirekha2.domain.executors.MainThread;
import com.webinfotech.santirekha2.domain.interactors.FetchReviewsInteractor;
import com.webinfotech.santirekha2.domain.interactors.base.AbstractInteractor;
import com.webinfotech.santirekha2.domain.models.Review.Review;
import com.webinfotech.santirekha2.domain.models.Review.ReviewsResponseWrapper;
import com.webinfotech.santirekha2.repository.ProductRepository.impl.GetProductsRepositoryImpl;

/**
 * Created by Raj on 28-02-2019.
 */

public class FetchReviewsInteractorImpl extends AbstractInteractor implements FetchReviewsInteractor {

    Callback mCallback;
    GetProductsRepositoryImpl mRepository;

    public FetchReviewsInteractorImpl(Executor threadExecutor,
                                      MainThread mainThread,
                                      Callback callback,
                                      GetProductsRepositoryImpl repository
                                      ) {
        super(threadExecutor, mainThread);
        this.mRepository = repository;
        mCallback = callback;
    }

    private void notifyError() {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingReviewsFail();
            }
        });
    }

    private void postMessage(final Review[] reviews) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingReviewsSuccess(reviews);
            }
        });
    }

    @Override
    public void run() {

        final ReviewsResponseWrapper responseWrapper = mRepository.fetchReviews();
        if (responseWrapper == null){
            notifyError();
        } else if(!responseWrapper.status){
            notifyError();
        } else {
            postMessage(responseWrapper.reviews);
        }
    }
}
