package com.webinfotech.santirekha2.domain.interactors;
/**
 * Created by Raj on 09-01-2019.
 */

public interface AddToCartInteractor {
    interface Callback {
        void onAddToCartSuccess(String successMsg);
        void onAddToCartFail(String errorMsg);
    }
}
