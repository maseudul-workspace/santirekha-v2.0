package com.webinfotech.santirekha2.domain.interactors;

public interface UpdateShippingAddressInteractor {
    interface Callback {
        void onEditAddressSuccess();
        void onEditAddressFail(String errorMsg);
    }
}
