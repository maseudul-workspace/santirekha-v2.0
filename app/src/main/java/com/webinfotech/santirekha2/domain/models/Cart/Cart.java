package com.webinfotech.santirekha2.domain.models.Cart;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Raj on 19-02-2019.
 */

public class Cart {
    @SerializedName("cart_id")
    @Expose
    public int cartId;

    @SerializedName("p_name")
    @Expose
    public String productName;

    @SerializedName("p_image")
    @Expose
    public String image;

    @SerializedName("p_stock")
    @Expose
    public int stock;

    @SerializedName("price")
    @Expose
    public double price;

    @SerializedName("mrp")
    @Expose
    public double mrp;

    @SerializedName("cash_back")
    @Expose
    public double cashBack;

    @SerializedName("promotional_bonus")
    @Expose
    public double promotionalBonus;

    @SerializedName("cart_quantity")
    @Expose
    public int cartQty;

    @SerializedName("p_is_star_product")
    @Expose
    public int isStarProduct;

}
