package com.webinfotech.santirekha2.domain.models.ImageSliders;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.webinfotech.santirekha2.domain.models.ImageSliders.ImageSlider;

/**
 * Created by Raj on 24-06-2019.
 */

public class ImageSliderWrapper {
    @SerializedName("status")
    @Expose
    public Boolean status;

    @SerializedName("message")
    @Expose
    public String message;

    @SerializedName("data")
    @Expose
    public ImageSlider[] imageSliders;
}
