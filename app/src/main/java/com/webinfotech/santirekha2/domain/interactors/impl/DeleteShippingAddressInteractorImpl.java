package com.webinfotech.santirekha2.domain.interactors.impl;

import android.content.Context;

import com.webinfotech.santirekha2.domain.executors.Executor;
import com.webinfotech.santirekha2.domain.executors.MainThread;
import com.webinfotech.santirekha2.domain.interactors.DeleteShippingAddressInteractor;
import com.webinfotech.santirekha2.domain.interactors.base.AbstractInteractor;
import com.webinfotech.santirekha2.domain.models.User.DeleteShippingAddressResponse;
import com.webinfotech.santirekha2.repository.User.UserRepositoryImpl;

public class DeleteShippingAddressInteractorImpl extends AbstractInteractor implements DeleteShippingAddressInteractor {

    UserRepositoryImpl mRepository;
    Callback mCallback;
    int userId;
    String apiKey;
    int addressId;

    public DeleteShippingAddressInteractorImpl(Executor threadExecutor, MainThread mainThread, UserRepositoryImpl mRepository, Callback mCallback, int userId, String apiKey, int addressId) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.userId = userId;
        this.apiKey = apiKey;
        this.addressId = addressId;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onAddressDeleteFail(errorMsg);
            }
        });
    }

    private void postMessage(){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onAddressDeleteSuccess();
            }
        });
    }

    @Override
    public void run() {
        final DeleteShippingAddressResponse deleteShippingAddressResponse = mRepository.deleteShippingAddress(userId, apiKey, addressId);
        if (deleteShippingAddressResponse == null) {
            notifyError("Something Went Wrong");
        } else if (!deleteShippingAddressResponse.status) {
            notifyError(deleteShippingAddressResponse.message);
        } else {
            postMessage();
        }
    }
}
