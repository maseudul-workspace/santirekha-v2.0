package com.webinfotech.santirekha2.domain.interactors;

import com.webinfotech.santirekha2.domain.models.Wallet.WalletDetails;

public interface GetCreditHistoryInteractor {
    interface Callback {
        void onGettingCreditHistorySuccess(WalletDetails walletDetails, int totalPage);
        void onGettingCreditHistoryFail(String errorMsg);
    }
}
