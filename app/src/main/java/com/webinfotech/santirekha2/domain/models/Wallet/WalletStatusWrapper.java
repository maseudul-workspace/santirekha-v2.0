package com.webinfotech.santirekha2.domain.models.Wallet;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.webinfotech.santirekha2.domain.models.Wallet.WalletStatus;

/**
 * Created by Raj on 21-02-2019.
 */

public class WalletStatusWrapper {

    @SerializedName("status")
    @Expose
    public Boolean status;

    @SerializedName("message")
    @Expose
    public String message;

    @SerializedName("code")
    @Expose
    public int code;

    @SerializedName("data")
    @Expose
    public WalletStatus walletStatus;

}
