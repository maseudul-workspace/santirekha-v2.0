package com.webinfotech.santirekha2.domain.interactors;

import com.webinfotech.santirekha2.domain.models.Review.Review;

/**
 * Created by Raj on 28-02-2019.
 */

public interface FetchReviewsInteractor {
    interface Callback{
        void onGettingReviewsSuccess(Review[] reviews);
        void onGettingReviewsFail();
    }
}
