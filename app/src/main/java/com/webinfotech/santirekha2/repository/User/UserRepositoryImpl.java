package com.webinfotech.santirekha2.repository.User;

import android.util.Log;

import com.google.gson.Gson;
import com.webinfotech.santirekha2.domain.models.Cart.Discount;
import com.webinfotech.santirekha2.domain.models.CommonResponse;
import com.webinfotech.santirekha2.domain.models.Orders.OrderHistoryWrapper;
import com.webinfotech.santirekha2.domain.models.Orders.OrderPlacedResponse;
import com.webinfotech.santirekha2.domain.models.Review.ReviewSubmitResponse;
import com.webinfotech.santirekha2.domain.models.StarProductsCountResponse;
import com.webinfotech.santirekha2.domain.models.User.DeleteShippingAddressResponse;
import com.webinfotech.santirekha2.domain.models.User.DownlineWrapper;
import com.webinfotech.santirekha2.domain.models.User.ForgotPasswordResponse;
import com.webinfotech.santirekha2.domain.models.User.PasswordChangeResponse;
import com.webinfotech.santirekha2.domain.models.User.ShippingAddressAddResponse;
import com.webinfotech.santirekha2.domain.models.User.ShippingAddressWrapper;
import com.webinfotech.santirekha2.domain.models.User.UpdateShippingAddressResponse;
import com.webinfotech.santirekha2.domain.models.User.UpdateUserResponse;
import com.webinfotech.santirekha2.domain.models.User.UserDetailsWrapper;
import com.webinfotech.santirekha2.domain.models.User.UserInfoWrapper;
import com.webinfotech.santirekha2.domain.models.User.UserStatusCheckResponse;
import com.webinfotech.santirekha2.domain.models.Wallet.WalletDetailsWrapper;
import com.webinfotech.santirekha2.domain.models.Wallet.WalletStatusWrapper;
import com.webinfotech.santirekha2.repository.ApiClient;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by Raj on 12-02-2019.
 */

public class UserRepositoryImpl {
    UserRepository mRepository;

    public UserRepositoryImpl() {
        mRepository = ApiClient.createService(UserRepository.class);
    }

    public CommonResponse signUp(String name,
                                 String mobile,
                                 String email,
                                 String password,
                                 String state,
                                 String city,
                                 String address,
                                 String referalCode,
                                 String pin){
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> createUser = mRepository.createUser(name, mobile, email, password, state, city, address, referalCode, pin);

            Response<ResponseBody> response = createUser.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);

                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
        }
        return commonResponse;
    }

    public UserInfoWrapper logInUser(String email, String password){
        UserInfoWrapper userInfoWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> logIn = mRepository.userLogIn(email, password);
            Response<ResponseBody> response = logIn.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    userInfoWrapper = null;
                }else{
                    userInfoWrapper = gson.fromJson(responseBody, UserInfoWrapper.class);

                }
            } else {
                userInfoWrapper = null;
            }
        }catch (Exception e){
            userInfoWrapper = null;
        }
        return userInfoWrapper;
    }

    public UserDetailsWrapper fetchUserDetails(int userId, String apiKey){
        UserDetailsWrapper userDetailsWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetchUser = mRepository.fetchUserDetails(userId, apiKey);
            Response<ResponseBody> response = fetchUser.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    userDetailsWrapper = null;
                }else{
                    userDetailsWrapper = gson.fromJson(responseBody, UserDetailsWrapper.class);

                }
            } else {
                userDetailsWrapper = null;
            }
        }catch (Exception e){
            userDetailsWrapper = null;
        }
        return userDetailsWrapper;
    }

    public UpdateUserResponse updateUser(int userId,
                                         String apiKey,
                                         String name,
                                         String email,
                                         String state,
                                         String city,
                                         String address,
                                         String pin){

        UpdateUserResponse updateUserResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> updateUser = mRepository.updateUser(userId, apiKey, name, email, state, city, address, pin);
            Response<ResponseBody> response = updateUser.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    updateUserResponse = null;
                }else{
                    updateUserResponse = gson.fromJson(responseBody, UpdateUserResponse.class);

                }
            } else {
                updateUserResponse = null;
            }
        }catch (Exception e){
            updateUserResponse = null;
        }
        return updateUserResponse;
    }

    public PasswordChangeResponse changePassword(String newPassword,
                                                 String oldPassword,
                                                 String apiKey,
                                                 int userId){
        PasswordChangeResponse passwordChangeResponse;

        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> changePassword = mRepository.changePassword(newPassword, oldPassword, userId, apiKey);
            Response<ResponseBody> response = changePassword.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    passwordChangeResponse = null;
                }else{
                    passwordChangeResponse = gson.fromJson(responseBody, PasswordChangeResponse.class);

                }
            } else {
                passwordChangeResponse = null;
            }
        }catch (Exception e){
            passwordChangeResponse = null;
        }

        return passwordChangeResponse;
    }

    public ShippingAddressWrapper fetchShippingAddress(int userId, String apiKey){
        ShippingAddressWrapper shippingAddressWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchShippingAddresses(userId, apiKey);
            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    shippingAddressWrapper = null;
                }else{
                    shippingAddressWrapper = gson.fromJson(responseBody, ShippingAddressWrapper.class);

                }
            } else {
                shippingAddressWrapper = null;
            }
        }catch (Exception e){
            shippingAddressWrapper = null;
        }

        return shippingAddressWrapper;
    }

    public ShippingAddressAddResponse addShippingAddress(int userId,
                                                         String apiKey,
                                                         String mobile,
                                                         String email,
                                                         String state,
                                                         String city,
                                                         String address,
                                                         String pin) {
        ShippingAddressAddResponse addressAddResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> addAddress = mRepository.addShippingAddress(userId, apiKey, mobile, email, state, city, address, pin);
            Response<ResponseBody> response = addAddress.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    addressAddResponse = null;
                }else{
                    addressAddResponse = gson.fromJson(responseBody, ShippingAddressAddResponse.class);

                }
            } else {
                addressAddResponse = null;
            }
        }catch (Exception e){
            addressAddResponse = null;
        }

        return addressAddResponse;
    }

    public UpdateShippingAddressResponse updateShippingAddress(int userId,
                                                               String apiKey,
                                                               int addressId,
                                                               String mobile,
                                                               String email,
                                                               String state,
                                                               String city,
                                                               String address,
                                                               String pin){
        UpdateShippingAddressResponse updateShippingAddressResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> updateAddress = mRepository.updateShippingAddress(userId, apiKey, addressId, mobile, email, state, city, address, pin);
            Response<ResponseBody> response = updateAddress.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    updateShippingAddressResponse = null;
                }else{
                    updateShippingAddressResponse = gson.fromJson(responseBody, UpdateShippingAddressResponse.class);

                }
            } else {
                updateShippingAddressResponse = null;
            }
        }catch (Exception e){
            updateShippingAddressResponse = null;
        }

        return updateShippingAddressResponse;
    }

    public DeleteShippingAddressResponse deleteShippingAddress(int userId, String apiKey, int addressId){
        DeleteShippingAddressResponse deleteShippingAddressResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> delete = mRepository.deleteShippingAddresses(userId, apiKey, addressId);
            Response<ResponseBody> response = delete.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    deleteShippingAddressResponse = null;
                }else{
                    deleteShippingAddressResponse = gson.fromJson(responseBody, DeleteShippingAddressResponse.class);

                }
            } else {
                deleteShippingAddressResponse = null;
            }
        }catch (Exception e){
            deleteShippingAddressResponse = null;
        }

        return deleteShippingAddressResponse;
    }

    public WalletStatusWrapper getWalletStatus(String apiKey, int userid){
        WalletStatusWrapper walletStatusWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.getWalletStatus(userid, apiKey);
            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    walletStatusWrapper = null;
                }else{
                    walletStatusWrapper = gson.fromJson(responseBody, WalletStatusWrapper.class);

                }
            } else {
                walletStatusWrapper = null;
            }
        }catch (Exception e){
            walletStatusWrapper = null;
        }
        return  walletStatusWrapper;
    }

    public WalletDetailsWrapper getWalletHistory(String apiKey, int userid, int page){
        WalletDetailsWrapper walletDetailsWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetchHistory = mRepository.getWalletHistory(userid, apiKey, page);
            Response<ResponseBody> response = fetchHistory.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    walletDetailsWrapper = null;
                }else{
                    walletDetailsWrapper = gson.fromJson(responseBody, WalletDetailsWrapper.class);

                }
            } else {
                walletDetailsWrapper = null;
            }
        }catch (Exception e){
            walletDetailsWrapper = null;
        }
        return  walletDetailsWrapper;
    }

    public OrderHistoryWrapper getOrderHistory(String apiKey, int userId){
        OrderHistoryWrapper orderHistoryWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetchOrderHistory = mRepository.getOrderHistory(userId, apiKey);
            Response<ResponseBody> response = fetchOrderHistory.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    orderHistoryWrapper = null;
                }else{
                    orderHistoryWrapper = gson.fromJson(responseBody, OrderHistoryWrapper.class);

                }
            } else {
                orderHistoryWrapper = null;
            }
        }catch (Exception e){
            orderHistoryWrapper = null;
        }
        return orderHistoryWrapper;
    }

    public Discount getDiscount(String apiKey, int userId){
        Discount discountResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> discount = mRepository.getDiscount(userId, apiKey);
            Response<ResponseBody> response = discount.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    discountResponse = null;
                }else{
                    discountResponse = gson.fromJson(responseBody, Discount.class);

                }
            } else {
                discountResponse = null;
            }
        }catch (Exception e){
            discountResponse = null;
        }
        return discountResponse;
    }

    public OrderPlacedResponse placeOrder(String apiKey, int userId, int walletStatus, int shippingAddressId){
        OrderPlacedResponse orderPlacedResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> placeOrder = mRepository.placeOrder(userId, apiKey, walletStatus, shippingAddressId);
            Response<ResponseBody> response = placeOrder.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    orderPlacedResponse = null;
                }else{
                    orderPlacedResponse = gson.fromJson(responseBody, OrderPlacedResponse.class);

                }
            } else {
                orderPlacedResponse = null;
            }
        }catch (Exception e){
            orderPlacedResponse = null;
        }
        return orderPlacedResponse;
    }

    public DownlineWrapper fetchDownlineList(String apiKey, int userId){
        DownlineWrapper downlineWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchDownline(userId, apiKey);
            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    downlineWrapper = null;
                }else{
                    downlineWrapper = gson.fromJson(responseBody, DownlineWrapper.class);
                }
            } else {
                downlineWrapper = null;
            }
        }catch (Exception e){
            downlineWrapper = null;
        }
        return downlineWrapper;
    }

    public ReviewSubmitResponse submitReview(String apiKey, int userId, int star, String comments){
        ReviewSubmitResponse reviewSubmitResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;

        try {
            Call<ResponseBody> submit = mRepository.submitReview(userId, apiKey, star, comments);
            Response<ResponseBody> response = submit.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    reviewSubmitResponse = null;
                }else{
                    reviewSubmitResponse = gson.fromJson(responseBody, ReviewSubmitResponse.class);
                }
            } else {
                reviewSubmitResponse = null;
            }
        }catch (Exception e){
            reviewSubmitResponse = null;
        }
        return reviewSubmitResponse;
    }

    public UserStatusCheckResponse checkUserStatus(int userId){
        UserStatusCheckResponse statusCheckResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> check = mRepository.checkUserStatus(userId);
            Response<ResponseBody> response = check.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    statusCheckResponse = null;
                }else{
                    statusCheckResponse = gson.fromJson(responseBody, UserStatusCheckResponse.class);
                }
            } else {
                statusCheckResponse = null;
            }
        }catch (Exception e){
            statusCheckResponse = null;
        }
        return statusCheckResponse;
    }

    public ForgotPasswordResponse forgotPassword(String email){
        ForgotPasswordResponse forgotPasswordResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> submit = mRepository.forgotPassword(email);
            Response<ResponseBody> response = submit.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    forgotPasswordResponse = null;
                }else{
                    forgotPasswordResponse = gson.fromJson(responseBody, ForgotPasswordResponse.class);
                }
            } else {
                forgotPasswordResponse = null;
            }
        }catch (Exception e){
            forgotPasswordResponse = null;
        }
        return forgotPasswordResponse;
    }

    public StarProductsCountResponse fetchStartProductCount(String apiKey, int userId) {
        StarProductsCountResponse starProductsCountResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchStarProductsCount(userId, apiKey);
            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    starProductsCountResponse = null;
                }else{
                    starProductsCountResponse = gson.fromJson(responseBody, StarProductsCountResponse.class);
                }
            } else {
                starProductsCountResponse = null;
            }
        }catch (Exception e){
            starProductsCountResponse = null;
        }
        return starProductsCountResponse;
    }

    public WalletDetailsWrapper getCreditHistory(String apiKey, int userid, int page){
        WalletDetailsWrapper walletDetailsWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetchHistory = mRepository.getCreditHistory(userid, apiKey, page);
            Response<ResponseBody> response = fetchHistory.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    walletDetailsWrapper = null;
                }else{
                    walletDetailsWrapper = gson.fromJson(responseBody, WalletDetailsWrapper.class);

                }
            } else {
                walletDetailsWrapper = null;
            }
        }catch (Exception e){
            walletDetailsWrapper = null;
        }
        return  walletDetailsWrapper;
    }

}
