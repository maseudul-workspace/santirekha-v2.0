package com.webinfotech.santirekha2.repository.ProductRepository;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by Raj on 05-01-2019.
 */

public interface GetProductsRepository {

    @POST("product/product_list_cat_wise.php")
    @FormUrlEncoded
    Call<ResponseBody> getProductListByCategory(@Field("type") int type,
                                                @Field("page_no") int pageNo,
                                                @Field("cat_id") int categoryId);

    @POST("product/product_details.php")
    @FormUrlEncoded
    Call<ResponseBody> getProductDetailsById(@Field("product_id") int productId);

    @GET("app_load/app_load.php")
    Call<ResponseBody> getMainData();

    @POST("product/product_search.php")
    @FormUrlEncoded
    Call<ResponseBody> searchProducts(@Field("search_key") String search_key,
                                      @Field("page_no") int page_no
    );

    @POST("review/customer_review_fetch.php")
    Call<ResponseBody> fetchReviews();

    @POST("user/star_product_purchase_list.php")
    @FormUrlEncoded
    Call<ResponseBody> fetchStarProducts(@Field("user_id") int userId,
                                         @Field("api_key") String apiKey,
                                         @Field("page_no") int pageNo
    );

}
