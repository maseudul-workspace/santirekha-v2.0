package com.webinfotech.santirekha2.repository.User;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by Raj on 12-02-2019.
 */

public interface UserRepository {

    @POST("user/user_create.php")
    @FormUrlEncoded
    Call<ResponseBody> createUser(@Field("name") String name,
                                  @Field("mobile") String mobile,
                                  @Field("email") String email,
                                  @Field("password") String password,
                                  @Field("state") String state,
                                  @Field("city") String city,
                                  @Field("address") String address,
                                  @Field("referal_code") String referalCode,
                                  @Field("pin") String pin
    );

    @POST("user/user_login_check.php")
    @FormUrlEncoded
    Call<ResponseBody> userLogIn(@Field("email") String email,
                                 @Field("password") String password
    );

    @POST("api/user/user_profile_fetch.php")
    @FormUrlEncoded
    Call<ResponseBody> fetchUserDetails(@Field("user_id") int userId,
                                        @Field("api_key") String apiKey
    );

    @POST("api/user/user_profile_update.php")
    @FormUrlEncoded
    Call<ResponseBody> updateUser(@Field("user_id") int userId,
                                  @Field("api_key") String apiKey,
                                  @Field("name") String name,
                                  @Field("email") String email,
                                  @Field("state") String state,
                                  @Field("city") String city,
                                  @Field("address") String address,
                                  @Field("pin") String pin);

    @POST("user/user_password_change.php")
    @FormUrlEncoded
    Call<ResponseBody> changePassword(@Field("new_password") String newPassword,
                                      @Field("current_password") String currentPassword,
                                      @Field("user_id") int userId,
                                      @Field("api_key") String apiKey
    );


    @POST("user/shipping_address_fetch.php")
    @FormUrlEncoded
    Call<ResponseBody> fetchShippingAddresses(@Field("user_id") int userId,
                                              @Field("api_key") String apiKey);

    @POST("user/add_new_shipping_address.php")
    @FormUrlEncoded
    Call<ResponseBody> addShippingAddress(@Field("user_id") int userId,
                                          @Field("api_key") String apiKey,
                                          @Field("mobile") String mobile,
                                          @Field("email") String email,
                                          @Field("state") String state,
                                          @Field("city") String city,
                                          @Field("address") String address,
                                          @Field("pin") String pin
    );

    @POST("user/update_shipping_address.php")
    @FormUrlEncoded
    Call<ResponseBody> updateShippingAddress(@Field("user_id") int userId,
                                             @Field("api_key") String apiKey,
                                             @Field("s_address_id") int addressId,
                                             @Field("mobile") String mobile,
                                             @Field("email") String email,
                                             @Field("state") String state,
                                             @Field("city") String city,
                                             @Field("address") String address,
                                             @Field("pin") String pin
    );

    @POST("user/delete_shipping_address.php")
    @FormUrlEncoded
    Call<ResponseBody> deleteShippingAddresses(@Field("user_id") int userId,
                                               @Field("api_key") String apiKey,
                                               @Field("s_address_id") int s_address_id
    );

    @POST("wallet/wallet_amount_fetch.php")
    @FormUrlEncoded
    Call<ResponseBody> getWalletStatus(@Field("user_id") int userId,
                                       @Field("api_key") String apiKey);

    @POST("wallet/wallet_history.php")
    @FormUrlEncoded
    Call<ResponseBody> getWalletHistory(@Field("user_id") int userId,
                                        @Field("api_key") String apiKey,
                                        @Field("page") int page
                                        );

    @POST("order/order_history.php")
    @FormUrlEncoded
    Call<ResponseBody> getOrderHistory(@Field("user_id") int userId,
                                       @Field("api_key") String apiKey);


    @POST("api/discount/discount_fetch.php")
    @FormUrlEncoded
    Call<ResponseBody> getDiscount(@Field("user_id") int userId,
                                   @Field("api_key") String apiKey);

    @POST("order/order_place.php")
    @FormUrlEncoded
    Call<ResponseBody> placeOrder(@Field("user_id") int userId,
                                  @Field("api_key") String apiKey,
                                  @Field("wallet_status") int wallet_status,
                                  @Field("shipping_address_id") int shipping_address_id
    );

    @POST("user/fetch_downline.php")
    @FormUrlEncoded
    Call<ResponseBody> fetchDownline(@Field("user_id") int userId,
                                     @Field("api_key") String apiKey);

    @POST("review/customer_review.php")
    @FormUrlEncoded
    Call<ResponseBody> submitReview(@Field("user_id") int userId,
                                    @Field("api_key") String apiKey,
                                    @Field("star") int star,
                                    @Field("comments") String comments);

    @POST("api/user/user_status_check.php")
    @FormUrlEncoded
    Call<ResponseBody> checkUserStatus(@Field("user_id") int userId);

    @POST("user/forgot_password.php")
    @FormUrlEncoded
    Call<ResponseBody> forgotPassword(@Field("email") String email);

    @POST("user/star_purchase_number.php")
    @FormUrlEncoded
    Call<ResponseBody> fetchStarProductsCount(@Field("user_id") int userId,
                                              @Field("api_key") String apiKey);

    @POST("user/credit_history.php")
    @FormUrlEncoded
    Call<ResponseBody> getCreditHistory(@Field("user_id") int userId,
                                        @Field("api_key") String apiKey,
                                        @Field("page") int page
    );


}
