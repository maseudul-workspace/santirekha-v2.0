package com.webinfotech.santirekha2.repository.ProductRepository.impl;

import com.google.gson.Gson;
import com.webinfotech.santirekha2.domain.models.Cart.CartAddedReponse;
import com.webinfotech.santirekha2.domain.models.Cart.CartDeleteResponse;
import com.webinfotech.santirekha2.domain.models.Cart.CartListWrapper;
import com.webinfotech.santirekha2.domain.models.Cart.CartUpdateResponse;
import com.webinfotech.santirekha2.repository.ApiClient;
import com.webinfotech.santirekha2.repository.ProductRepository.CartRepository;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by Raj on 09-01-2019.
 */

public class CartRepositoryImpl {
    private CartRepository mRepository;

    public CartRepositoryImpl() {
        mRepository = ApiClient.createService(CartRepository.class);
    }

    public CartAddedReponse addToCart(String apiKey,
                                      int userId,
                                      int productId,
                                      int quantity){
        CartAddedReponse addedReponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try{
            Call<ResponseBody> addToCart = mRepository.addToCart(apiKey, userId, productId, quantity);
            Response<ResponseBody> response = addToCart.execute();
            if (response.body() != null) {
                responseBody = response.body().string();
            } else if (response.errorBody() != null) {
                responseBody = response.errorBody().string();
                isErrorResponse = true;
            }

            if (responseBody != null && !responseBody.isEmpty()) {
                if (isErrorResponse) {
                    addedReponse = null;
                } else {
                    addedReponse = gson.fromJson(responseBody, CartAddedReponse.class);
                }
            } else {
                addedReponse = null;
            }

        } catch (Exception e){
            addedReponse = null;
        }
        return addedReponse;
    }

    public CartUpdateResponse updateCart(int userId, String apiKey, int cartId, int quantity){
        CartUpdateResponse updateResponse = null;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try{
            Call<ResponseBody> update = mRepository.updateCart(apiKey, userId, cartId, quantity);
            Response<ResponseBody> response = update.execute();
            if (response.body() != null) {
                responseBody = response.body().string();

            } else if (response.errorBody() != null) {
                responseBody = response.errorBody().string();
                isErrorResponse = true;

            }

            if (responseBody != null && !responseBody.isEmpty()) {
                if (isErrorResponse) {
                    updateResponse = null;
                } else {
                    updateResponse = gson.fromJson(responseBody, CartUpdateResponse.class);
                }
            } else {
                updateResponse = null;
            }

        } catch (Exception e){

            updateResponse = null;
        }
        return updateResponse;
    }

    public CartListWrapper getCartDetails(int userId, String apiKey){
        CartListWrapper cartListWrapper = null;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try{
            Call<ResponseBody> getCartList = mRepository.getCartDetails(apiKey, userId);
            Response<ResponseBody> response = getCartList.execute();
            if (response.body() != null) {
                responseBody = response.body().string();

            } else if (response.errorBody() != null) {
                responseBody = response.errorBody().string();
                isErrorResponse = true;

            }

            if (responseBody != null && !responseBody.isEmpty()) {
                if (isErrorResponse) {
                    cartListWrapper = null;
                } else {
                    cartListWrapper = gson.fromJson(responseBody, CartListWrapper.class);
                }
            } else {
                cartListWrapper = null;
            }

        } catch (Exception e){

            cartListWrapper = null;
        }
        return cartListWrapper;
    }

    public CartDeleteResponse deleteCartItem(int userId, String apiKey, int cartId){
        CartDeleteResponse cartDeleteResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try{
            Call<ResponseBody> delete = mRepository.deleteCartItem(apiKey, userId, cartId);
            Response<ResponseBody> response = delete.execute();
            if (response.body() != null) {
                responseBody = response.body().string();

            } else if (response.errorBody() != null) {
                responseBody = response.errorBody().string();
                isErrorResponse = true;

            }

            if (responseBody != null) {
                if (isErrorResponse) {
                    cartDeleteResponse = null;
                } else {

                    cartDeleteResponse = gson.fromJson(responseBody, CartDeleteResponse.class);
                }
            } else {
                cartDeleteResponse = null;
            }

        } catch (Exception e){
            cartDeleteResponse = null;
        }
        return cartDeleteResponse;
    }

}
