package com.webinfotech.santirekha2.repository.ProductRepository;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by Raj on 09-01-2019.
 */

public interface CartRepository {

    @POST("cart/add_cart.php")
    @FormUrlEncoded
    Call<ResponseBody> addToCart(
            @Field("api_key") String apiKey,
            @Field("user_id") int userId,
            @Field("product_id") int productId,
            @Field("quantity") int quantity
    ) ;

    @POST("cart/edit_cart_item.php")
    @FormUrlEncoded
    Call<ResponseBody> updateCart(@Field("api_key") String apiKey,
                                  @Field("user_id") int userId,
                                  @Field("cart_id") int cartId,
                                  @Field("quantity") int quantity) ;

    @POST("cart/fetch_cart_products.php")
    @FormUrlEncoded
    Call<ResponseBody> getCartDetails(
            @Field("api_key") String apiKey,
            @Field("user_id") int userId
    );

    @POST("cart/delete_cart_item.php")
    @FormUrlEncoded
    Call<ResponseBody> deleteCartItem(@Field("api_key") String apiKey,
                                      @Field("user_id") int userId,
                                      @Field("cart_id") int cartId);

}
